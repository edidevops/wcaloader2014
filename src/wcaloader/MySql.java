/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */

package wcaloader;

import com.edi.common.Logging;
import com.edi.common.MySqlConnection.MySqlConnection;
import com.edi.common.dates;
import com.edi.common.shell;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MySql {

    private shell   dosShell = new shell();
    private Logging log = new Logging("Automation");
    private MySqlConnection mysql = null;
    private Connection conn = null;
    private String user = null;
    private String pass = null;
    private String database = null;
    private String host = null;
    private String hostname = null;
    private int bAutoRun = 0;
    private String LogFilePath = "";
    private int Load = 0;
    private ArrayList PK = new ArrayList();
    private String ActTime = "";
    private String ActTimeOld = "";
    private String record = "";
    private String LTbl = "";
    private int insert = 0;
    private int update = 0;
    private int err = 0;
    private String[] fldName = null;
    private String[] NullFld = null;
    private String[] ChkFld = null;
    private String NoOfPKFld = "";
    private String runonload = "";
    private boolean TblExist = false;
    private boolean TblFormat = false;
    
    public MySql(){
        
    }
    
    
    
    public boolean initialization(String Tuser, String Tpass, String Tdatabase, String Thost, int Auto, String LFilePath, String RunOnLoad, String Thostname){
            user = Tuser;
            pass = Tpass;
            database = Tdatabase;
            host = Thost;
            hostname = Thostname;
            runonload = RunOnLoad;
            if(OpenDBConnection()){
                bAutoRun = Auto;
                LogFilePath = LFilePath;
                ActTime = GetTimeStamp();
                ActTimeOld = GetTimeStamp();
                if(ActTime != null){
                    if(ActTime.length()< 0) ActTime = new dates().getJapDate(new java.util.Date());
                }else{
                    ActTime = new dates().getJapDate(new java.util.Date());
                }
                return true;
            }else{ 
                return false;
            }
    }
            
    
    private boolean OpenDBConnection(){
        try{
            mysql = new MySqlConnection(user,pass,database,host);
            conn = mysql.getConnection();
            return true;
       }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")OpenDBConnection: " + e.getMessage());
            }else{
                log.append("(MySql: " + hostname + "/" + host + ")OpenDBConnection: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
            return false;
       }
    }
     
    public void MyLoading(String row){
          try{              
              record = row;
              Load = 0;              
              String[] Sfld = row.split("\\|");
              if(Sfld != null){
                  for(int c=0;c<Sfld.length;c++){
                      if(Sfld[c]!=null){
                          Sfld[c] = Sfld[c].trim();
                          if(c == 1)Sfld[c]= Sfld[c].toLowerCase();
                      }else{
                          Sfld[c] = "";
                      }
                  }
                  
                  //checking for _my tables
                  if(Sfld[1].equalsIgnoreCase("div") || Sfld[1].equalsIgnoreCase("int") || Sfld[1].equalsIgnoreCase("call")){
                      Sfld[1] = Sfld[1] + "_my";
                  }
                  // rename parallel table to EDI new z prefixed name
                  //javax.swing.JOptionPane.showMessageDialog(null,Sfld[1]);
                  if(Sfld[1].equalsIgnoreCase("bochg") || Sfld[1].equalsIgnoreCase("bschg") || Sfld[1].equalsIgnoreCase("crchg") || Sfld[1].equalsIgnoreCase("ctchg") || Sfld[1].equalsIgnoreCase("ffc") || Sfld[1].equalsIgnoreCase("frnfx") || Sfld[1].equalsIgnoreCase("icc") || Sfld[1].equalsIgnoreCase("ifchg") || Sfld[1].equalsIgnoreCase("irchg") || Sfld[1].equalsIgnoreCase("ischg") || Sfld[1].equalsIgnoreCase("lcc") || Sfld[1].equalsIgnoreCase("lstat") || Sfld[1].equalsIgnoreCase("ltchg") || Sfld[1].equalsIgnoreCase("mtchg") || Sfld[1].equalsIgnoreCase("rconv") || Sfld[1].equalsIgnoreCase("rdnom") || Sfld[1].equalsIgnoreCase("scchg") || Sfld[1].equalsIgnoreCase("shoch") || Sfld[1].equalsIgnoreCase("trchg")){
                     Sfld[1] = "z" + Sfld[1];
                     //javax.swing.JOptionPane.showMessageDialog(null,Sfld[1]);
                  }            
                  
                  if(LTbl.equals(Sfld[1])){ // Check for table if it is the same 
                      if(TblFormat){
                          if(TblExist){
                              String[] fldValue = new String[Sfld.length - 3];
                              int cf = 0;
                              String [] PKsplt = NoOfPKFld.split(",");
                              for(int c=2;c < Sfld.length - 1;c++){
                                  fldValue[cf] = Sfld[c];
                                  cf++;
                                  for(int c1=0;c1<PKsplt.length;c1++){
                                      if(c == Integer.parseInt(PKsplt[c1])){
                                          String [] val = PK.get(c1).toString().split("=");
                                          String fldval = val[0] + " = '" + Sfld[c] +"'";
                                          PK.set(c1, fldval);                                      
                                      }
                                  }
                              }

                              if(ChkNotNullFld(fldValue) && CheckFields(fldValue)){
                                   //Checking for the Insert/Update on primary key
                                       if(PK.size() > 0){
                                            ChkPK(Sfld[1], PK);
                                       }else{
                                            if (bAutoRun==0 || bAutoRun==1){
                                                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")Primary Key not found for table: " + Sfld[1] + "\n" + row);
                                            }else{
                                                log.append("(MySql: " + hostname + "/" + host + ")Primary Key not found for table: " + Sfld[1] + "\n" + row,  LogFilePath, "WCALoader");                               
                                            }
                                            err++;
                                       }

                                   //Insert/Update
                                   if(Load ==1){
                                       UpdateRec(Sfld[1], fldName, fldValue, PK);
                                   }else if(Load == 2){                               
                                       InsertRec(Sfld[1], fldName, fldValue);
                                   }
                              }
                          }else{
                              if (bAutoRun==0 || bAutoRun==1){
                                    javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")Table not exists: " + Sfld[1] + "\n" + row);
                              }else{
                                    log.append("(MySql: " + hostname + "/" + host + ")Table not exists: " + Sfld[1] + "\n" + row,  LogFilePath, "WCALoader");                               
                              }
                              err++;
                          }
                      }
                  }else{ // Check for table if it is not the same 
                      LTbl =  Sfld[1];                  
                      Statement Tst = conn.createStatement();
                      ResultSet Trs = Tst.executeQuery(/*" USE " + database  + " ;" +*/
                                                       " SELECT * FROM INFORMATION_SCHEMA.TABLES " +
                                                       " WHERE TABLE_TYPE = 'BASE TABLE' " +
                                                       " AND TABLE_NAME = '" + Sfld[1] + "'" +
                                                       " AND TABLE_SCHEMA = '"+database+"'");
                      Trs.next();
                      if(Trs.getRow() > 0){
                           TblExist = true;
                           Statement Dst = conn.createStatement();
                           ResultSet Drs = Dst.executeQuery(/*"USE " + database  + " ;" +*/
                                                            " SELECT * FROM INFORMATION_SCHEMA.Columns "
                                                            + "WHERE TABLE_NAME = '" + Sfld[1] + "'"
                                                            + " AND TABLE_SCHEMA = '"+database+"'" +
                                                            " ORDER BY ORDINAL_POSITION" );
                           Drs.last();                       
                           if(Sfld.length == (Drs.getRow() - 2)){
                               TblFormat = true;
                               Drs.beforeFirst();
                               NoOfPKFld = "";
                               PK.clear();                           
                               fldName = new String[Sfld.length - 3];
                               NullFld = new String[Sfld.length - 3];
                               ChkFld = new String[Sfld.length - 3];
                               String[] fldValue = new String[Sfld.length - 3];
                               int cnt = 0;
                               int cnt_fld = 0;
                               while(Drs.next()){ 
                                   if(cnt>=5){
                                       if(Drs.getString("COLUMN_KEY") != null){
                                           if(Drs.getString("COLUMN_KEY").length()>0){    
                                               if(Drs.getString("COLUMN_KEY").compareToIgnoreCase("PRI") == 0){
                                                    PK.add(Drs.getString("COLUMN_NAME")+ " = '" + Sfld[cnt_fld+2] + "'");
                                                    NoOfPKFld += (cnt_fld + 2) + ",";  
                                               }                                           
                                           }//Key length
                                       }//Key Null
                                           
                                       ChkFld[cnt_fld] = Drs.getString("DATA_TYPE");
                                       NullFld[cnt_fld] = Drs.getString("IS_NULLABLE");
                                       fldName[cnt_fld] = Drs.getString("COLUMN_NAME");
                                       fldValue[cnt_fld] = Sfld[cnt_fld+2];                                   
                                       
                                       cnt_fld++;
                                   }   
                                   cnt++;
                               }//End for loop
                               Drs.close();
                               Dst.close();                           

                               if(ChkNotNullFld(fldValue) && CheckFields(fldValue)){
                                   //Checking for the Insert/Update on primary key
                                   if(PK.size() > 0){
                                        ChkPK(Sfld[1], PK);
                                   }else{
                                        if (bAutoRun==0 || bAutoRun==1){
                                            javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")Primary Key not found for table: " + Sfld[1] + "\n" + row);
                                        }else{
                                            log.append("(MySql: " + hostname + "/" + host + ")Primary Key not found for table: " + Sfld[1] + "\n" + row,  LogFilePath, "WCALoader");                               
                                        }
                                        err++;
                                   }

                                   //Insert/Update
                                   if(Load ==1){
                                       UpdateRec(Sfld[1], fldName, fldValue, PK);
                                   }else if(Load == 2){                               
                                       InsertRec(Sfld[1], fldName, fldValue);
                                   }                                                               
                               }
                           }else{
                               TblFormat = false;
                                if (bAutoRun==0 || bAutoRun==1){
                                    javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ") Difference in the number of Columns between file and table " + Sfld[1]+ "\n" + record);
                                }else{
                                    log.append("(MySql: " + hostname + "/" + host + ") Difference in the number of Columns between file and table " + Sfld[1]+ "\n" + record,  LogFilePath, "WCALoader");                               
                                }
                                err++;
                           }//length of the fields
                      }else{
                          TblExist = false;
                          if (bAutoRun==0 || bAutoRun==1){
                                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")Table not exists: " + Sfld[1] + "\n" + row);
                          }else{
                                log.append("(MySql: " + hostname + "/" + host + ")Table not exists: " + Sfld[1] + "\n" + row,  LogFilePath, "WCALoader");                               
                          }
                          err++;
                      }//check table exists                  
                      Trs.close();
                      Tst.close();
                  }
              }
          }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")MyLoading: " + e.getMessage()+ "\n" + row);
            }else{
                log.append("(MySql: " + hostname + "/" + host + ")MyLoading: " + e.getMessage()+ "\n" + row,  LogFilePath, "WCALoader");                               
            }   
            err++;
          }
    }
    
    public void changeDB(String dbName){
        database = dbName;
    }
    
    private void InsertRec(String tbl, String[] FName, String[] FVal){
        try{
            int GCAction = -1;
            String ActFlag = "I";
            boolean Binsert = false;
            String sqlcmd = "INSERT INTO " + database + "." + tbl + "( acttime,actflag,";
            for(int f1=0;f1<FName.length;f1++){
                if(FName[f1].equalsIgnoreCase("GCAction"))    GCAction = f1;
                sqlcmd += FName[f1] + ",";
            }
            
            //Determine Actflag
            if(FVal[GCAction].equalsIgnoreCase("A"))  ActFlag = "I";
            else if(FVal[GCAction].equalsIgnoreCase("U"))  ActFlag = "I";
            else ActFlag = FVal[GCAction];
            
            sqlcmd =sqlcmd.substring(0, sqlcmd.length() - 1) + ") VALUES ( '" + ActTime + "','" + ActFlag + "'," ;
            for(int f1=0;f1<FVal.length;f1++){
                if(f1 == GCAction){
                    if(FVal[f1].equalsIgnoreCase("D")){
                       Binsert = false;
                       break;
                    }else{
                       Binsert = true;
                    }
                }
                FVal[f1] = FVal[f1].replace("'", "`");
                FVal[f1] = FVal[f1].replace("\"", "`");
                FVal[f1] = FVal[f1].replace("\\", "`");
                FVal[f1] = FVal[f1].replace("\t", " ");
                sqlcmd += "'" + FVal[f1] + "',";
            }
            
            if(Binsert){
                sqlcmd = sqlcmd.substring(0, sqlcmd.length() - 1) + ")";
                sqlcmd = sqlcmd.replace("'null'", "null");
                Statement Insertst = conn.createStatement();
                int update_result = Insertst.executeUpdate(sqlcmd);
                Insertst.close();
                if(update_result<=0){
                    log.append("Could not load\n" + record,  LogFilePath, "WCALoader");
                }else{
                    insert++;
                }
            }else{
               // log.append("(MySql: " + hostname + "/" + host + ") New record marked deleted" + "\n" + record,  LogFilePath, "WCALoader");
            }
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)InsertRec: " + e.getMessage() + "\n" + record);
            }else{
                log.append("(MySql: " + hostname + "/" + host + ")InsertRec: " + e.getMessage() + "\n" + record,  LogFilePath, "WCALoader");
            }   
            err++;
        }
    }
    
      private void UpdateRec(String tbl, String[] FName, String[] FVal, ArrayList PKFld){
        try{
            int GCAction = -1;
            String ActFlag = "U";
            for(int f1=0;f1<FName.length;f1++){
               if(FName[f1].equalsIgnoreCase("GCAction"))    GCAction = f1;               
            }
            
            //Determine Actflag
            if(FVal[GCAction].equalsIgnoreCase("A"))  ActFlag = "U";            
            else ActFlag = FVal[GCAction];
            
            String sqlcmd = "UPDATE "+ database +  "." + tbl + " SET acttime = '" + ActTime + "', actflag = '" + ActFlag + "', ";
            for(int f1=0;f1<FName.length;f1++){
                FVal[f1] = FVal[f1].replace("'", "`");
                FVal[f1] = FVal[f1].replace("\"", "`");
                FVal[f1] = FVal[f1].replace("\\", "`");
                FVal[f1] = FVal[f1].replace("\t", " ");
                sqlcmd += FName[f1]  + "= '" + FVal[f1]  + "', ";
            }
            sqlcmd =sqlcmd.substring(0, sqlcmd.length() - 2) + " WHERE ";
            for(int f2=0 ; f2<PKFld.size(); f2++){
                sqlcmd += PKFld.get(f2).toString() + " AND ";
            }
            sqlcmd = sqlcmd.substring(0, sqlcmd.length() - 4);            
            sqlcmd = sqlcmd.replace("'null'", "null");
            Statement Updatest = conn.createStatement();
            int update_result = Updatest.executeUpdate(sqlcmd);
            Updatest.close();
            if(update_result<=0){
                log.append("(MySql: " + hostname + "/" + host + ")Could not update\n"+ record,  LogFilePath, "WCALoader");
            }else{
                update++;
            }            
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")UpdateRec: " + e.getMessage() + "\n" + record);
            }else{
                log.append("(MySql: " + hostname + "/" + host + ")UpdateRec: " + e.getMessage() + "\n" + record,  LogFilePath, "WCALoader");                               
            }   
            err++;
        }
    }
    
    private void ChkPK(String Tbl, ArrayList fld){
        try{
            String sql = "SELECT COUNT(*) AS cnt FROM " + database + "." + Tbl + " WHERE ";
            for(int f2=0 ; f2<fld.size(); f2++){
                sql += fld.get(f2).toString() + " AND ";
            }
            sql = sql.substring(0, sql.length() - 4); 
            Statement PKst = conn.createStatement();
            ResultSet PKrs = PKst.executeQuery(sql);
            PKrs.next();
            if(PKrs.getRow() > 0){
                if (PKrs.getInt("cnt") > 0){
                    Load = 1; //Update
                }else{
                    Load = 2; //Insert
                } 
            }else{
                Load = 0;
            }
            PKst.close();
            PKrs.close();
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")ChkPK: " + e.getMessage() + "\n" + record);
            }else{
                log.append("(MySql: " + hostname + "/" + host + ")ChkPK: " + e.getMessage() + "\n" + record,  LogFilePath, "WCALoader");                               
            }
            err++;
            Load = 0;
        }
    }
    
    public String GetLogFileName(){
        try{
            String name = "";
            if(chkLogCnt()){
                LogCul();
                Statement Logst = conn.createStatement();
                ResultSet Logrs = Logst.executeQuery("SELECT " +
                                " CASE WHEN (SUBSTRING_INDEX(MAX(file_name), '.', -1) = 'z01') THEN CONCAT(SUBSTRING_INDEX(MAX(file_name), '.', 1),'.z02') " +
                                "  " +
                                "      WHEN (SUBSTRING_INDEX(MAX(file_name), '.', -1) = 'z02') THEN CONCAT(SUBSTRING_INDEX(MAX(file_name), '.', 1),'.z03') " +                                                     
                                "  " +
                                "      WHEN (SUBSTRING_INDEX(MAX(file_name), '.', -1) = 'z03') THEN CONCAT(SUBSTRING_INDEX(MAX(file_name), '.', 1),'.z04') " +                                                     
                                "  " +
                                "      WHEN (SUBSTRING_INDEX(MAX(file_name), '.', -1) = 'z04' AND DAYNAME(SUBSTRING_INDEX(MAX(file_name), '.', 1)) LIKE 'Friday') " +
                                "      THEN CONCAT(REPLACE(ADDDATE(SUBSTRING_INDEX(MAX(file_name), '.', 1),3),'-',''),'.z01') " +
                                "  " +
                                "      WHEN (SUBSTRING_INDEX(MAX(file_name), '.', -1) = 'z04') THEN CONCAT(REPLACE(ADDDATE(SUBSTRING_INDEX(MAX(file_name), '.', 1),1),'-',''),'.z01') " +
                                "  " +
                                " ELSE '' END AS 'filename' " +
                                " FROM " + database + ".tbl_opslog " +
                                " WHERE mode like 'Daily'");
                Logrs.next();
                if(Logrs.getRow()>0){
                    name = Logrs.getString("filename").trim();                
                }else{
                    name =  "";
                }            
                Logrs.close();
                Logst.close();  
            }else{
                name =  "";
            }
            return name;
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")chkLog: " + e.getMessage());
            }else{
                log.append("(MySql: " + hostname + "/" + host + ")chkLog: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
            return "Dont Load";
        }
    }
    
    private void LogCul(){
        try{
            Statement Culst = conn.createStatement();
            Culst.executeUpdate(" DELETE FROM " + database + ".tbl_opslog " +
                                " WHERE acttime < SUBDATE(curdate(),31)");   
            Culst.close();
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)LogCul: " + e.getMessage());
            }else{
                log.append("(MySql: " + hostname + "/" + host + ")LogCul: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
        }
    }
    
     private boolean chkLogCnt(){
        try{
            boolean Rcnt = true;
            Statement Cntst = conn.createStatement();
            ResultSet Cntrs = Cntst.executeQuery("SELECT COUNT(*) AS cnt FROM " + database + ".tbl_opslog");
            Cntrs.next();
            if(Cntrs.getInt("cnt") > 0){
                Rcnt = true;
            }else{
                Rcnt = false;
            }
            Cntrs.close();
            Cntst.close();            
            return Rcnt;
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")chkLogCnt: " + e.getMessage());
            }else{
                log.append("(MySql: " + hostname + "/" + host + ")chkLogCnt: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
            return true;
        }
    }
     
   public int InsertLog(String mode, String fname){
       int tcount = insert + update +err;
      try{
          String result = "", FeedDate = "";
          int seq = 0;
          int seqold = 0;
          
          if(err>0){
              result = "File has been loaded with errors";
          }else{
              result = "File has been loaded successfully";
          }
          
          FeedDate = fname.substring(0, 4) + "-" + fname.substring(4, 6) + "-" + fname.substring(6, 8);
          seq = Integer.parseInt(fname.substring(fname.length()-1, fname.length()));
          seqold = seq-1;

          if (seq == 2) {
             ActTimeOld = GetPrevTimeStamp();
          }
          
          Statement Intst = conn.createStatement();
          int load = Intst.executeUpdate("INSERT INTO " + database + ".tbl_opslog (file_name, acttime, mode, insert_cnt, update_cnt, error_cnt, result, feeddate, seq, acttime_new, seq_new)" +
           "VALUES ('" + fname + "', '" + ActTimeOld + "', '" + mode + "', " + insert + ", " + update + ", " + err + ", '" + result + "', '" + FeedDate + "', " + seqold + ", '" + ActTime + "', " + seq + ")");   
           
          if(load <= 0){
                if (bAutoRun==0 || bAutoRun==1){
                    javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")InsertLog:: Could not log file: " + fname);
                }else{
                    log.append("(MySql: " + hostname + "/" + host + ")InsertLog:  Could not log file: " + fname,  LogFilePath, "WCALoader");                               
                }
          }else{
              if(runonload!= null){
                    if(runonload.length() > 0) dosShell.RunIt(runonload);
              }
          }
          Intst.close();
      }catch(Exception e){
         if (bAutoRun==0 || bAutoRun==1){
            javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")InsertLog: Error loging file: " + fname + "\t" + e.getMessage());
         }else{
            log.append("(MySql: " + hostname + "/" + host + ")InsertLog: Error loging file: " + fname + "\t" + e.getMessage(),  LogFilePath, "WCALoader");                               
         } 
      }
      return tcount;
  }
   
   
   
   private String GetTimeStamp(){
      try{
          String TimeStamp = "";
          Statement TSst = conn.createStatement();
                ResultSet TSrs = TSst.executeQuery("SELECT NOW() AS ActTime");
                if(TSrs != null){
                    TSrs.next();                    
                    if(TSrs.getRow()>0){
                        TimeStamp = TSrs.getString("ActTime").trim();
                    }else{
                        TimeStamp = "";
                    }
                }else{
                    TimeStamp = "";
                }
                TSrs.close();
                TSst.close();  
          return TimeStamp;
      }catch(Exception e){
         if (bAutoRun==0 || bAutoRun==1){
            javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")GetTimeStamp: "+ e.getMessage());
         }else{
            log.append("(MySql: " + hostname + "/" + host + ")GetTimeStamp: " + e.getMessage(),  LogFilePath, "WCALoader");                               
         } 
         return ""; 
      }
   }
   
   private String GetPrevTimeStamp(){
      try{
          String TimeStamp = "";
          Statement TSst = conn.createStatement();
                ResultSet TSrs = TSst.executeQuery("SELECT max(acttime) as acttime from wca.tbl_opslog where seq_new=1");
                if(TSrs != null){
                    TSrs.next();                    
                    if(TSrs.getRow()>0){
                        TimeStamp = TSrs.getString("acttime").trim();
                    }else{
                        TimeStamp = "";
                    }
                }else{
                    TimeStamp = "";
                }
                TSrs.close();
                TSst.close();  
          return TimeStamp;
      }catch(Exception e){
         if (bAutoRun==0 || bAutoRun==1){
            javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")GetPrevTimeStamp: "+ e.getMessage());
         }else{
            log.append("(MySql: " + hostname + "/" + host + ")GetPrevTimeStamp: " + e.getMessage(),  LogFilePath, "WCALoader");                               
         } 
         return ""; 
      }
   }
   
   private boolean ChkNotNullFld(String[] fldVal){
       try{
           for(int n=0;n<NullFld.length;n++){
               if(NullFld[n].compareToIgnoreCase("YES") != 0){
                   if(fldVal[n].equalsIgnoreCase("")){
                        if (bAutoRun==0 || bAutoRun==1){
                               javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ") Non-Null key " + fldName[n] + " has 'NULL' value\n" + record);
                        }else{
                               log.append("(MySql: " + hostname + "/" + host + ") Non-Null key " + fldName[n] + " has 'NULL' value\n" + record,  LogFilePath, "WCALoader");                               
                        }
                        err++;
                        return false;
                   }                                                
               }
           }
           return true;
       }catch(Exception e){
             if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")ChkNotNullFld: "+ e.getMessage());
             }else{
                log.append("(MySql: " + hostname + "/" + host + ")ChkNotNullFld: " + e.getMessage(),  LogFilePath, "WCALoader");                               
             } 
           err++;
           return false;
       }
   }
   
   private boolean CheckFields(String[] fldVal){
       String fld = "";
       try{          
           for(int n=0;n<ChkFld.length;n++){
               //if(!ChkFld[n].toUpperCase().startsWith("CHAR") && !ChkFld[n].toUpperCase().startsWith("VARCHAR") && !ChkFld[n].toUpperCase().startsWith("TEXT")){
               if(!ChkFld[n].toUpperCase().startsWith("CHAR") && !ChkFld[n].toUpperCase().startsWith("VARCHAR")){
                   if(ChkFld[n].compareToIgnoreCase("datetime") == 0 || ChkFld[n].compareToIgnoreCase("date") == 0 ){ 
                        if(fldVal[n].length()>0){                   
                           SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                           fld = fldName[n] + "--->" +fldVal[n];
                           if(sdf.parse(fldVal[n]).before(sdf.parse("19010101"))){
                               /* if (bAutoRun==0 || bAutoRun==1){
                                       javax.swing.JOptionPane.showMessageDialog(null,"(MySql) Date changed to null: " + fldName[n] + "\n" + record);
                                }else{
                                       log.append("(MySql)Date changed to null: " + fldName[n] + "\n" + record,  LogFilePath, "WCALoader");                               
                                }
                                err++;*/
                                fldVal[n] = "null";
                                //return false;
                                //return true;
                           }                                                
                       }else{
                           fld = fldName[n] + "--->" +fldVal[n];
                           fldVal[n] = "null";
                       }
                   }else{
                       if(fldVal[n].length()<=0){
                           if(ChkFld[n].toUpperCase().startsWith("TEXT")){
                                fld = fldName[n] + "--->" +fldVal[n];
                                fldVal[n] = "No further information";
                           }else{
                                fld = fldName[n] + "--->" +fldVal[n];
                                fldVal[n] = "null";
                           }
                       }
                   }
               }
           }
           return true;
       }catch(Exception e){
             if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql: " + hostname + "/" + host + ")CheckValidDate: "+ e.getMessage() + "\n "+ fld + "\n" + record );
             }else{
                log.append("(MySql: " + hostname + "/" + host + ")CheckValidDate: " + e.getMessage() + "\n" + fld + "\n" + record,  LogFilePath, "WCALoader");                               
             } 
           err++;
           return false;
       }
            
   }
      
    public void Closeconn(){
        conn = null;
    }

    private Object equalsIgnoreCase(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
