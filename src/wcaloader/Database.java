/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcaloader;

public class Database {

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getTblName() {
        return tblName;
    }

    public void setTblName(String tblName) {
        this.tblName = tblName;
    }
    
    private String dbName;
    private String tblName;
    
        @Override
    public int hashCode() {
        int hash = 0;
        hash += (tblName != null ? tblName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Database)) {
            return false;
        }
        Database other = (Database) object;
        if ((this.tblName == null && other.tblName != null) || (this.tblName != null && !this.tblName.equalsIgnoreCase(other.tblName))) {
            return false;
        }
        return true;
    }
    
}
