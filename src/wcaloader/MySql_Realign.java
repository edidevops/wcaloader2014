/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */

package wcaloader;

import com.edi.common.Logging;
import com.edi.common.MySqlConnection.MySqlConnection;
import com.edi.common.dates;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MySql_Realign {

    private Logging log = new Logging("Automation");
    private MySqlConnection mysql = null;
    private Connection conn = null;
    private String user = null;
    private String pass = null;
    private String database = null;
    private String host = null;
    private int bAutoRun = 0;
    private String LogFilePath = "";
    private String record = "";
    private String LTbl = "";
    private int insert = 0;
    private int err = 0;
    private String[] fldName = null;
    private String[] NullFld = null;
    private String[] ChkFld = null;
    public boolean TblExist = false;
    public boolean isTblRecDel = false;
    private String logsrv = "";
    
    public MySql_Realign(){
        
    }
    
    public boolean initialization(String Tuser, String Tpass, String Tdatabase, String Thost, int Auto, String LFilePath, String lsrv){                    
            user = Tuser;
            pass = Tpass;
            database = Tdatabase;
            host = Thost;
            logsrv = lsrv;
            if(OpenDBConnection()){
                bAutoRun = Auto;
                LogFilePath = LFilePath;               
                return true;
            }else{ 
                return false;
            }
    }
            
    
    private boolean OpenDBConnection(){
        try{
            mysql = new MySqlConnection(user,pass,database,host);
            conn = mysql.getConnection();
            return true;
       }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)OpenDBConnection: " + e.getMessage());
            }else{
                log.append("(MySql)OpenDBConnection: " + e.getMessage(),  LogFilePath, logsrv + "_WCALoader_Realign");                               
            }
            return false;
       }
    }
     
    public void MyLoading(String row, int count){
          try{              
              record = row;           
              String[] Sfld = row.split("\\|");
              if(Sfld != null){
                  for(int c=0;c<Sfld.length;c++){
                      if(Sfld[c]!=null){
                          Sfld[c] = Sfld[c].trim();
                          if(c == 1)Sfld[c]= Sfld[c].toLowerCase();
                      }else{
                          Sfld[c] = "";
                      }
                  }
                  
                  //checking for _my tables
                  if(Sfld[1].equalsIgnoreCase("div") || Sfld[1].equalsIgnoreCase("int") || Sfld[1].equalsIgnoreCase("call")){
                      Sfld[1] = Sfld[1] + "_my";
                  }
                  
                  if(LTbl.equals(Sfld[1])){ // Check for table if it is the same 
                      if(TblExist){
                          String[] fldValue = new String[Sfld.length - 3];
                          int cf = 0;
                          for(int c=2;c < Sfld.length - 1;c++){
                              fldValue[cf] = Sfld[c];
                              cf++;                             
                          }
                          
                          //Load record
                          if(ChkNotNullFld(fldValue) && CheckFields(fldValue)){                                                          
                                InsertRec(Sfld[1], fldName, fldValue);                               
                          }
                      }else{
                          if (bAutoRun==0 || bAutoRun==1){
                                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)Table not exists: " + Sfld[1] + "\n" + row);
                          }else{
                                log.append("(MySql)Table not exists: " + Sfld[1] + "\n" + row,  LogFilePath, logsrv + "_WCALoader_Realign");                               
                          }
                          err++;
                      }

                  }else if(count == 1){ // Check for table if it is not the same
                      
                          LTbl =  Sfld[1];                  
                          Statement Tst = conn.createStatement();
                          ResultSet Trs = Tst.executeQuery("SHOW TABLES FROM wca LIKE '" + Sfld[1] + "'");
                          Trs.next();
                          if(Trs.getRow() > 0){
                              if(DelRecords(Sfld[1])){
                                   isTblRecDel = true;
                                   TblExist = true;
                                   Statement Dst = conn.createStatement();
                                   ResultSet Drs = Dst.executeQuery("DESCRIBE wca." + Sfld[1]);
                                   Drs.last();
                                   if(Sfld.length == (Drs.getRow() - 2)){
                                       Drs.beforeFirst();
                                       fldName = new String[Sfld.length - 3];
                                       NullFld = new String[Sfld.length - 3];
                                       ChkFld = new String[Sfld.length - 3];
                                       String[] fldValue = new String[Sfld.length - 3];
                                       int cnt = 0;
                                       int cnt_fld = 0;
                                       while(Drs.next()){
                                           if(cnt>=5){
                                               ChkFld[cnt_fld] = Drs.getString("Type");
                                               NullFld[cnt_fld] = Drs.getString("Null");
                                               fldName[cnt_fld] = Drs.getString("Field");
                                               fldValue[cnt_fld] = Sfld[cnt_fld+2];

                                               cnt_fld++;
                                           }
                                           cnt++;
                                       }//End for loop
                                       Drs.close();
                                       Dst.close();

                                       //Load record
                                       if(ChkNotNullFld(fldValue) && CheckFields(fldValue)){
                                            InsertRec(Sfld[1], fldName, fldValue);
                                       }
                                   }//length of the fields
                              }else{
                                  log.append("(MySql)Could not delete records: " + Sfld[1] ,  LogFilePath, logsrv + "_WCALoader_Realign");
                                  isTblRecDel = false;
                               }
                          }else{
                              TblExist = false;
                              if (bAutoRun==0 || bAutoRun==1){
                                    javax.swing.JOptionPane.showMessageDialog(null,"(MySql)Table not exists: " + Sfld[1] + "\n" + row);
                              }else{
                                    log.append("(MySql)Table not exists: " + Sfld[1] + "\n" + row,  LogFilePath, logsrv + "_WCALoader_Realign");                               
                              }
                              err++;
                          }//check table exists                  
                          Trs.close();
                          Tst.close();
                      
                  }else{
                      if (bAutoRun==0 || bAutoRun==1){
                            javax.swing.JOptionPane.showMessageDialog(null,"(MySql)Different Table: " + Sfld[1] + "\n" + row);
                      }else{
                            log.append("(MySql)Different Table: " + Sfld[1] + "\n" + row,  LogFilePath, logsrv + "_WCALoader_Realign");
                      }
                  }
              }
          }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)MyLoading: " + e.getMessage()+ "\n" + row);
            }else{
                log.append("(MySql)MyLoading: " + e.getMessage()+ "\n" + row,  LogFilePath, logsrv + "_WCALoader_Realign");                               
            }   
            err++;
          }
    }
    
    private void InsertRec(String tbl, String[] FName, String[] FVal){
        String sqlcmd = "INSERT INTO wca." + tbl + "( acttime,actflag,";
        try{
            int GCAction = -1, GCDate = -1, GCTime = -1;            
            for(int f1=0;f1<FName.length;f1++){
                if(FName[f1].equalsIgnoreCase("GCAction"))  GCAction = f1;
                if(FName[f1].equalsIgnoreCase("GCDate"))    GCDate = f1;
                if(FName[f1].equalsIgnoreCase("GCTime"))    GCTime = f1;
                sqlcmd += FName[f1] + ",";
            }

            String ActTime = "";
            try{
                ActTime = FVal[GCDate].substring(0, 4) + "-" +  FVal[GCDate].substring(4, 6) + "-" +  FVal[GCDate].substring(6, 8) + " " + FVal[GCTime];
            }catch(Exception e){
                if (bAutoRun==0 || bAutoRun==1){
                    javax.swing.JOptionPane.showMessageDialog(null,"(MySql)GCA control fields empty: " + e.getMessage() + "\n" + record);
                }else{
                    log.append("(MySql)GCA control fields empty: " + e.getMessage()+ "\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");
                    ActTime = new dates().getJapDate(new java.util.Date());
                    ActTime = ActTime.substring(0,10) + " 12:00:00";
                }
            }

             //Checking Actflag 
            if(FVal[GCAction].equalsIgnoreCase("A"))   FVal[GCAction] = "I";
            
            sqlcmd =sqlcmd.substring(0, sqlcmd.length() - 1) + ") VALUES ( '" + ActTime + "', '" + FVal[GCAction] + "'," ;
            for(int f1=0;f1<FVal.length;f1++){
                FVal[f1] = FVal[f1].replace("'", "`");
                FVal[f1] = FVal[f1].replace("\"", "`");
                FVal[f1] = FVal[f1].replace("\\", "`");
                FVal[f1] = FVal[f1].replace("\t", " ");
                sqlcmd += "'" + FVal[f1] + "',";
            }
                      
            sqlcmd = sqlcmd.substring(0, sqlcmd.length() - 1) + ")";
            sqlcmd = sqlcmd.replace("'null'", "null");
            //sqlcmd = sqlcmd.replace("''", "null");
            Statement Insertst = conn.createStatement();
            int update_result = Insertst.executeUpdate(sqlcmd);
            Insertst.close();
            if(update_result<=0){
                log.append("Could not load\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");
            }else{
                insert++;
            }

        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)InsertRec: " + e.getMessage() + "\n" + record);
            }else{
                log.append("(MySql)InsertRec: " + e.getMessage() + "\n" + record + "\n" + sqlcmd,  LogFilePath, logsrv + "_WCALoader_Realign");
            }   
            err++;
        }
    }
         
        
   private String GetTimeStamp(){
      try{
          String TimeStamp = "";
          Statement TSst = conn.createStatement();
                ResultSet TSrs = TSst.executeQuery("SELECT NOW() AS ActTime");
                if(TSrs != null){
                    TSrs.next();                    
                    if(TSrs.getRow()>0){
                        TimeStamp = TSrs.getString("ActTime").trim();
                    }else{
                        TimeStamp = "";
                    }
                }else{
                    TimeStamp = "";
                }
                TSrs.close();
                TSst.close();  
          return TimeStamp;
      }catch(Exception e){
         if (bAutoRun==0 || bAutoRun==1){
            javax.swing.JOptionPane.showMessageDialog(null,"(MySql)GetTimeStamp: "+ e.getMessage());
         }else{
            log.append("(MySql)GetTimeStamp: " + e.getMessage(),  LogFilePath, logsrv + "_WCALoader_Realign");                               
         } 
         return ""; 
      }
   }
   
   private boolean ChkNotNullFld(String[] fldVal){
       try{
           for(int n=0;n<NullFld.length;n++){
               if(NullFld[n].compareToIgnoreCase("YES") != 0){
                   if(fldVal[n].equalsIgnoreCase("")){
                        if (bAutoRun==0 || bAutoRun==1){
                               javax.swing.JOptionPane.showMessageDialog(null,"(MySql) Non-Null key " + fldName[n] + " has 'NULL' value\n" + record);
                        }else{
                               log.append("(MySql) Non-Null key " + fldName[n] + " has 'NULL' value\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");                               
                        }
                        err++;
                        return false;
                   }                                                
               }
           }
           return true;
       }catch(Exception e){
             if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)ChkNotNullFld: "+ e.getMessage());
             }else{
                log.append("(MySql)ChkNotNullFld: " + e.getMessage(),  LogFilePath, logsrv + "_WCALoader_Realign");                               
             } 
           err++;
           return false;
       }
   }
   
   private boolean CheckFields(String[] fldVal){
       String fld = "";
       try{          
           for(int n=0;n<ChkFld.length;n++){
               if(!ChkFld[n].toUpperCase().startsWith("CHAR") && !ChkFld[n].toUpperCase().startsWith("VARCHAR")){
                   if(ChkFld[n].compareToIgnoreCase("datetime") == 0 || ChkFld[n].compareToIgnoreCase("date") == 0 ){ 
                        if(fldVal[n].length()>0){                   
                           SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                           fld = fldName[n] + "--->" +fldVal[n];
                           if(sdf.parse(fldVal[n]).before(sdf.parse("19010101"))){
                               /* if (bAutoRun==0 || bAutoRun==1){
                                       javax.swing.JOptionPane.showMessageDialog(null,"(MySql) Date changed to null: " + fldName[n] + "\n" + record);
                                }else{
                                       log.append("(MySql)Date changed to null: " + fldName[n] + "\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");                               
                                }
                                err++;*/
                                fldVal[n] = "null";
                                //return false;
                           }                                                
                       }else{
                           fld = fldName[n] + "--->" +fldVal[n];
                           fldVal[n] = "null";
                       }
                   }else{
                       if(fldVal[n].length()<=0){
                           if(ChkFld[n].toUpperCase().startsWith("TEXT")){
                                fld = fldName[n] + "--->" +fldVal[n];
                                fldVal[n] = "No further information";
                           }else{
                                fld = fldName[n] + "--->" +fldVal[n];
                                fldVal[n] = "null";
                           }
                       }
                   }
               }
           }
           return true;
       }catch(Exception e){
             if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)CheckValidDate: "+ e.getMessage() + "\n "+ fld + "\n" + record );
             }else{
                log.append("(MySql)CheckValidDate: " + e.getMessage() + "\n" + fld + "\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");                               
             } 
           err++;
           return false;
       }
            
   }
   
    private boolean DelRecords(String tbl){
       try{
           boolean rtn = false;
           Statement Dst = conn.createStatement();
           ResultSet Drs = Dst.executeQuery("SELECT COUNT(*) as cnt FROM " + tbl);
           if(Drs!=null){
               Drs.next();
               if(Drs.getInt("cnt") > 0){
                   int del = Dst.executeUpdate("DELETE FROM " + tbl);
                   if(del > 0) rtn = true;
                   else rtn = false;
               }else{
                   rtn = true;
               }
           }else{
               rtn = true;
           }
           Drs.close();
           Dst.close();
           return rtn;
       }catch(Exception e){
             if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MySql)DelRecords: "+ e.getMessage());
             }else{
                log.append("(MySql)DelRecords: " + e.getMessage(),  LogFilePath, logsrv + "_WCALoader_Realign");                               
             }              
             err++;
             return false;
       }
   }
      
    public void Closeconn(){
        conn = null;
    }

}
