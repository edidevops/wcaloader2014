/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */

package wcaloader;

import com.edi.common.Logging;
import com.edi.common.MsSqlConnection.MsSqlConnection;
import com.edi.common.dates;
import com.edi.common.shell;
import com.edi.gmailer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MsSql {

    private shell dosShell = new shell();
    private Logging log = new Logging("Automation");
    private MsSqlConnection mssql = null;
    private Connection conn = null;
    private String user = null;
    private String pass = null;
    private String database = null;
    private String host = null;
    private String hostname = null;
    private int bAutoRun = 0;
    private String LogFilePath = "";
    private int Load = 0;
    private ArrayList PK = new ArrayList();
    private String ActTime = "";
    private String ActTimeOld = "";
    private String record = "";
    private int insert = 0;
    private int update = 0;
    private int err = 0;
    private String[] fldName = null;
    private String[] ChkFld = null;
    private String NoOfPKFld = "";
    private String LTbl = "";
    private boolean TblExist = false;
    private boolean TblFormat = false;
    private String runonload = "";
    private boolean fullLoad = false;
    
    public MsSql(){
        
    }
    
    public boolean initialization(String Tuser, String Tpass, String Tdatabase, String Thost, int Auto, String LFilePath, String RunOnLoad, String Thostname){
        user = Tuser;
        pass = Tpass;
        database = Tdatabase;
        host = Thost;
        hostname = Thostname;
        runonload = RunOnLoad;
        if(OpenDBConnection()){                        
            bAutoRun = Auto;
            LogFilePath = LFilePath;
            ActTime = GetTimeStamp();
            ActTimeOld = GetTimeStamp();
            if(ActTime != null){
                if(ActTime.length()< 0) ActTime = new dates().getJapDate(new java.util.Date());
            }else{
                ActTime = new dates().getJapDate(new java.util.Date());
            }
            return true;
        }else{
            return false;
        }
    }
    
    private boolean OpenDBConnection(){
        try{
            mssql = new MsSqlConnection(user,pass,database,host);
            conn = mssql.getConnection();
            return true;
       }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")OpenDBConnection: " + e.getMessage());
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")OpenDBConnection: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
            return false;
       }
    }
     
    public void MsLoading(String row){
          try{
              record = row;
              Load = 0;
              String[] Sfld = row.split("\\|");
              if(Sfld != null){
                  for(int c=0;c<Sfld.length;c++){
                      if(Sfld[c]!=null){
                          Sfld[c] = Sfld[c].trim();
                          if(c == 1)Sfld[c]= Sfld[c].toLowerCase();
                      }else{
                          Sfld[c] = "";
                      }
                  }
                  // rename parallel table to EDI new z prefixed name
                  //javax.swing.JOptionPane.showMessageDialog(null,Sfld[1]);
                  if ("bochg".equals(Sfld[1]) || "bschg".equals(Sfld[1]) || "crchg".equals(Sfld[1]) || "ctchg".equals(Sfld[1]) || "ffc".equals(Sfld[1]) || "frnfx".equals(Sfld[1]) || "icc".equals(Sfld[1]) || "ifchg".equals(Sfld[1]) || "irchg".equals(Sfld[1]) || "ischg".equals(Sfld[1]) || "lcc".equals(Sfld[1]) || "lstat".equals(Sfld[1]) || "ltchg".equals(Sfld[1]) || "mtchg".equals(Sfld[1]) || "rconv".equals(Sfld[1]) || "rdnom".equals(Sfld[1]) || "scchg".equals(Sfld[1]) || "shoch".equals(Sfld[1]) || "trchg".equals(Sfld[1])){
                     Sfld[1] = "z" + Sfld[1];
                     //javax.swing.JOptionPane.showMessageDialog(null,Sfld[1]);
                  }            
                  

                  if(LTbl.equals(Sfld[1])){ // Check for table if it is the same 
                      if(TblFormat){
                          if(TblExist){
                              String[] fldValue = new String[Sfld.length - 3];
                              int cf = 0;
                              String [] PKsplt = NoOfPKFld.split(",");
                              for(int c=2;c < Sfld.length - 1;c++){
                                  fldValue[cf] = Sfld[c];
                                  cf++;
                                  for(int c1=0;c1<PKsplt.length;c1++){
                                      if(c == Integer.parseInt(PKsplt[c1])){
                                          String [] val = PK.get(c1).toString().split("=");
                                          String fldval = val[0] + " = '" + Sfld[c] +"'";
                                          PK.set(c1, fldval);                                      
                                      }
                                  }
                              }
                               //Checking for the Insert/Update on primary key
                                   if(PK.size() > 0){
                                        ChkPK(Sfld[1], PK);
                                   }else{
                                        if (bAutoRun==0 || bAutoRun==1){
                                            javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")Primary Key not found for table: " + Sfld[1] + "\n" + row);
                                        }else{
                                            log.append("(MsSql: " + hostname + "/" + host + ")Primary Key not found for table: " + Sfld[1] + "\n" + row,  LogFilePath, "WCALoader");                               
                                        }
                                        err++;
                                   }

                               //Insert/Update
                               if(Load ==1){
                                   CheckFields(fldValue);
                                   UpdateRec(Sfld[1], fldName, fldValue, PK, database);
                                   //if(fullLoad)UpdateRec("full_" + Sfld[1], fldName, fldValue, PK, "xwca");
                               }else if(Load == 2){    
                                   CheckFields(fldValue);
                                   InsertRec(Sfld[1], fldName, fldValue, database);
                                   //if(fullLoad)InsertRec("full_" + Sfld[1], fldName, fldValue, "xwca");
                               }            
                          }else{
                              if (bAutoRun==0 || bAutoRun==1){
                                    javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")Table not exists: " + Sfld[1] + "\n" + row);
                              }else{
                                    log.append("(MsSql: " + hostname + "/" + host + ")Table does not exist: " + Sfld[1] + "\n" + row,  LogFilePath, "WCALoader");                               
                              }
                              err++;
                          }
                      }
                  }else{ // Check for table if it is not the same 
                      LTbl =  Sfld[1];
                      fullLoad = false;
                      Statement Tst = conn.createStatement();
                      ResultSet Trs = Tst.executeQuery(" USE " + database  +
                                                       " SELECT * FROM INFORMATION_SCHEMA.TABLES " +
                                                       " WHERE TABLE_TYPE = 'BASE TABLE' " +
                                                       " AND TABLE_NAME = '" + Sfld[1] + "'");
                      Trs.next();
                      if(Trs.getRow() > 0){
                          TblExist = true;
                          //Get columns
                           Statement Cst = conn.createStatement();
                           ResultSet Crs = Cst.executeQuery("USE " + database  +
                                                            " SELECT * FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '" + Sfld[1] + "'" +
                                                            " ORDER BY ORDINAL_POSITION" );

                           fldName = new String[Sfld.length - 3];
                           ChkFld = new String[Sfld.length - 3];
                           String[] fldValue = new String[Sfld.length - 3];                      
                           if(Sfld.length == (GetColCnt(Sfld[1]) - 2)){
                               TblFormat = true;
                               //Get Fields
                               int pos = 0;
                               int cnt =0;
                               int cnt1 =2;
                               while(Crs.next()){
                                   if((pos = Crs.getInt("ORDINAL_POSITION")) > 5){
                                       fldName[cnt] = Crs.getString("COLUMN_NAME");
                                       ChkFld[cnt] = Crs.getString("DATA_TYPE");
                                       fldValue[cnt] = Sfld[cnt1];
                                       cnt++;
                                       cnt1++;
                                   }
                               }                                                           

                               //Get primary key
                               Statement Dst = conn.createStatement();
                               ResultSet Drs = Dst.executeQuery("USE " + database +
                                                                " SELECT [name],colid FROM syscolumns " +
                                                                " WHERE [id] IN (SELECT [id] FROM sysobjects WHERE [name] = '" + Sfld[1] + "') " +
                                                                " AND colid IN (SELECT SIK.colid FROM sysindexkeys SIK " +
                                                                "               JOIN sysobjects SO ON SIK.[id] = SO.[id] " +
                                                                "               WHERE SIK.indid = 1 AND SO.[name] = '" + Sfld[1] + "')");
                               NoOfPKFld = "";
                               PK.clear();                                                      
                               int cnt_fld = 0;
                               while(Drs.next()){    
                                       cnt_fld = Drs.getInt("colid");
                                       PK.add(Drs.getString("name")+ " = '" + Sfld[cnt_fld - 4] + "'");
                                       NoOfPKFld += (cnt_fld - 4) + ",";  
                               }//End for loop
                               Drs.close();
                               Dst.close();
                               
                                   //Checking for the Insert/Update on primary key
                                   if(PK.size() > 0){
                                        ChkPK(Sfld[1], PK);
                                   }else{
                                        if (bAutoRun==0 || bAutoRun==1){
                                            javax.swing.JOptionPane.showMessageDialog(null,"(MsSql:" + host + ")PKey not found for table: " + Sfld[1]+ "\n" + record);
                                        }else{
                                            log.append("(MsSql:" + host + ")PKey not found for table: " + Sfld[1]+ "\n" + record,  LogFilePath, "WCALoader");                               
                                        }
                                        err++;
                                   }

                                   //CheckFullLoad(Sfld[1], "xwca");

                                   //Insert/Update
                                   if(Load ==1){
                                       CheckFields(fldValue);
                                       UpdateRec(Sfld[1], fldName, fldValue, PK, database);
                                       //if(fullLoad)UpdateRec("full_" + Sfld[1], fldName, fldValue, PK, "xwca");
                                   }else if(Load == 2){                               
                                       CheckFields(fldValue);
                                       InsertRec(Sfld[1], fldName, fldValue, database);
                                       //if(fullLoad)InsertRec("full_" + Sfld[1], fldName, fldValue, "xwca");
                                   }

                           }else{
                               TblFormat = false;
                                if (bAutoRun==0 || bAutoRun==1){
                                    javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ") Difference in the number of Columns between file and table " + Sfld[1]+ "\n" + record);
                                }else{
                                    log.append("(MsSql:" + host + ") Difference in the number of Columns between file and table " + Sfld[1]+ "\n" + record,  LogFilePath, "WCALoader");                               
                                }
                                err++;
                           }//length of the fields
                           Crs.close();
                           Cst.close();                       


                           

                      }else{
                          if (bAutoRun==0 || bAutoRun==1){
                                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")Table does not exist: " + Sfld[1] + "\n" + row);
                          }else{
                                log.append("(MsSql: " + hostname + "/" + host + ")Table does not exist: " + Sfld[1] + "\n" + row,  LogFilePath, "WCALoader");                               
                          }      
                          err++;
                          TblExist = false;
                      }//check table exists
                      Trs.close();
                      Tst.close();                      
                  }
              }
          }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")MsLoading: " + e.getMessage() + "\n" + row);
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")MsLoading: " + e.getMessage()+ "\n" + row,  LogFilePath, "WCALoader");                               
            }   
            err++;
          }
    }

    private void CheckFullLoad(String ctbl, String db){
        try{
            //Check for full version of table is exists or not
            Statement  Tst = conn.createStatement();
            ResultSet  Trs = Tst.executeQuery(" USE " + db +
                                               " SELECT count(*) as cnt FROM INFORMATION_SCHEMA.TABLES " +
                                               " WHERE TABLE_TYPE = 'BASE TABLE' " +
                                               " AND TABLE_NAME = 'full_" + ctbl + "'");
              Trs.next();
              if(Trs.getRow() > 0){
                if(Trs.getInt("cnt") > 0){
                    fullLoad = true;
                }
              }
              Trs.close();
              Tst.close();
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")CheckFullLoad: " + e.getMessage());
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")CheckFullLoad: " + e.getMessage(),  LogFilePath, "WCALoader");
            }
        }
    }

    private void InsertRec(String tbl, String[] FName, String[] FVal, String db){
        try{
            int GCAction = -1;
            String ActFlag = "I";
            boolean Binsert = false;
            String sqlcmd = "USE " + db + " INSERT INTO " + tbl + "( acttime,actflag,";
            for(int f1=0;f1<FName.length;f1++){
               if(FName[f1].equalsIgnoreCase("GCAction"))    GCAction = f1;
               sqlcmd += FName[f1] + ",";
            }
            
            //Determine Actflag
            if(FVal[GCAction].equalsIgnoreCase("A"))  ActFlag = "I";
            else if(FVal[GCAction].equalsIgnoreCase("U"))  ActFlag = "I";
            else ActFlag = FVal[GCAction];
            
            sqlcmd =sqlcmd.substring(0, sqlcmd.length() - 1) + ") VALUES ( '" + ActTime + "','" + ActFlag + "'," ;
            for(int f1=0;f1<FVal.length;f1++){
                if(f1 == GCAction){
                    if(FVal[f1].equalsIgnoreCase("D")){
                       Binsert = false;
                       break;
                    }else{
                       Binsert = true;
                    }
                }
                FVal[f1] = FVal[f1].replace("'", "`");
                FVal[f1] = FVal[f1].replace("\"", "`");
                FVal[f1] = FVal[f1].replace("\\", "`");
                FVal[f1] = FVal[f1].replace("\t", " ");
                sqlcmd += "'" + FVal[f1] + "',";
            }
            
            if(Binsert){
                sqlcmd = sqlcmd.substring(0, sqlcmd.length() - 1) + ")";
                sqlcmd = sqlcmd.replace("'null'", "null");
                Statement Insertst = conn.createStatement();
                int update_result = Insertst.executeUpdate(sqlcmd);
                Insertst.close();
                if(update_result<=0){
                    err++;
                    log.append("(MsSql: " + hostname + "/" + host + ")Could not load\n" + record,  LogFilePath, "WCALoader");
                }else{
                    insert++;
                }    
            }else{
               // log.append("(MsSql) New record marked deleted" + "\n" + record,  LogFilePath, "WCALoader");
            }
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")InsertRec: " + e.getMessage() + "\n" + record);
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")InsertRec: " + e.getMessage()+ "\n" + record,  LogFilePath, "WCALoader");                               
            }   
            err++;
        }
    }
    
      private void UpdateRec(String tbl, String[] FName, String[] FVal, ArrayList PKFld, String db){
        try{
            int GCAction = -1;
            String ActFlag = "U";
            for(int f1=0;f1<FName.length;f1++){
               if(FName[f1].equalsIgnoreCase("GCAction"))    GCAction = f1;               
            }
            
            //Determine Actflag
            if(FVal[GCAction].equalsIgnoreCase("A"))  ActFlag = "U";            
            else ActFlag = FVal[GCAction];
            
            
            String sqlcmd = "USE " + db + " UPDATE " + tbl + " SET acttime = '" + ActTime + "', actflag = '" + ActFlag + "', ";
            for(int f1=0;f1<FName.length;f1++){
                FVal[f1] = FVal[f1].replace("'", "`");
                FVal[f1] = FVal[f1].replace("\"", "`");
                FVal[f1] = FVal[f1].replace("\\", "`");
                FVal[f1] = FVal[f1].replace("\t", " ");
                sqlcmd += FName[f1]  + "= '" + FVal[f1]  + "', ";
            }
            sqlcmd =sqlcmd.substring(0, sqlcmd.length() - 2) + " WHERE ";
            for(int f2=0 ; f2<PKFld.size(); f2++){
                sqlcmd += PKFld.get(f2).toString() + " AND ";
            }
            sqlcmd = sqlcmd.substring(0, sqlcmd.length() - 4);
            sqlcmd = sqlcmd.replace("'null'", "null");
            Statement Updatest = conn.createStatement();
            int update_result = Updatest.executeUpdate(sqlcmd);
            Updatest.close();
            if(update_result<=0){
                err++;
                log.append("(MsSql: " + hostname + "/" + host + ")Could not update\n" + record,  LogFilePath, "WCALoader");
            }else{
                update++;
            }            
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")UpdateRec: " + e.getMessage()+ "\n" + record);
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")UpdateRec: " + e.getMessage()+ "\n" + record,  LogFilePath, "WCALoader");                               
            }   
            err++;
        }
    }      
    
    private void ChkPK(String Tbl, ArrayList fld){
        try{
            String sql = "USE " + database + " SELECT COUNT(*) AS cnt FROM " + Tbl + " WHERE ";
            for(int f2=0 ; f2<fld.size(); f2++){
                sql += fld.get(f2).toString() + " AND ";
            }
            sql = sql.substring(0, sql.length() - 4); 
            Statement PKst = conn.createStatement();
            ResultSet PKrs = PKst.executeQuery(sql);
            PKrs.next();
            if(PKrs.getRow() > 0){
                if (PKrs.getInt("cnt") > 0){
                    Load = 1; //Update
                }else{
                    Load = 2; //Insert
                } 
            }else{
                Load = 0;
            }
            PKst.close();
            PKrs.close();
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")ChkPK: " + e.getMessage()+ "\n" + record);
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")ChkPK: " + e.getMessage()+ "\n" + record,  LogFilePath, "WCALoader");                               
            }
            err++;
            Load = 0;
        }
    }
    
    public void changeDB(String dbName){
        database = dbName;
    }
    
    public String GetLogFileName(){
        try{
            String name = "";
            if(chkLogCnt()){
                LogCul();
                Statement Logst = conn.createStatement();
                ResultSet Logrs = Logst.executeQuery("USE " + database  +
                                                     " SELECT " +
                                                     " CASE WHEN (RIGHT(MAX(file_name),3) = 'z01') THEN LEFT(MAX(file_name), 8) + '.z02' " +
                                                     " " +
                                                     "   WHEN (RIGHT(MAX(file_name),3) = 'z02') THEN LEFT(MAX(file_name), 8) + '.z03' " +                                                     
                                                     " " +
                                                     "   WHEN (RIGHT(MAX(file_name),3) = 'z03') THEN LEFT(MAX(file_name), 8) + '.z04' " +                                                     
                                                     " " +
                                                     "    WHEN (RIGHT(MAX(file_name),3) = 'z04' AND DATENAME(dw,LEFT(MAX(file_name), 8)) LIKE 'Friday')  " +
                                                     "    THEN CONVERT(VARCHAR,DATEADD(D,3,LEFT(MAX(file_name), 8)),112) + '.z01' " +
                                                     " " +
                                                     "    WHEN (RIGHT(MAX(file_name),3) = 'z04') THEN CONVERT(VARCHAR,DATEADD(D,1,LEFT(MAX(file_name), 8)),112) + '.z01' " +
                                                     " " +
                                                     " ELSE '' END AS 'filename'  " +
                                                     " FROM tbl_opslog  " +
                                                     " WHERE mode like 'Daily'");
                if(Logrs != null){
                    Logrs.next();                    
                    if(Logrs.getRow()>0){
                        name = Logrs.getString("filename").trim();
                    }else{
                        name = "";
                    }
                }else{
                    name = "";
                }
                Logrs.close();
                Logst.close();  
            }else{
                name = "";
            }
            return name;
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")chkLog: " + e.getMessage());
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")chkLog: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
            return "Dont Load";
        }
    }
    
    private void LogCul(){
        try{
            Statement Culst = conn.createStatement();
            Culst.executeUpdate("USE " + database + " DELETE FROM tbl_opslog " +
                                " WHERE acttime < (getdate() - 31)");   
            Culst.close();
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")LogCul: " + e.getMessage());
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")LogCul: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
        }
    }
    
   private boolean chkLogCnt(){
        try{
            boolean Rcnt = true;
            Statement Cntst = conn.createStatement();
            ResultSet Cntrs = Cntst.executeQuery("USE " + database + " SELECT COUNT(*) AS cnt FROM tbl_opslog");
            Cntrs.next();
            if(Cntrs.getInt("cnt") > 0){
                Rcnt = true;
            }else{
                Rcnt = false;
            }
            Cntrs.close();
            Cntst.close();            
            return Rcnt;
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")chkLogCnt: " + e.getMessage());
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")chkLogCnt: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
            return true;
        }
    }
   
   private int GetColCnt(String tbl){
       try{
           int cnt = 0;
           Statement Colst = conn.createStatement();
           ResultSet Colrs = Colst.executeQuery("USE " + database +
                                                " SELECT COUNT(*) AS cnt FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '" + tbl + "'");
           Colrs.next();  
           cnt = Colrs.getInt("cnt");
           Colst.close();
           Colrs.close();
           return cnt;
       }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")GetColCnt: " + e.getMessage());
            }else{
                log.append("(MsSql: " + hostname + "/" + host + ")GetColCnt: " + e.getMessage(),  LogFilePath, "WCALoader");                               
            }
            return 0;
       }
   }
   
  public int InsertLog(String mode, String fname){
      int tcount = insert + update + err;                    
      try{
          String result = "", FeedDate = "";
          int seq = 0;
          int seqold = 0;
          
          if(err>0){
              result = "File has been loaded with errors";
          }else{
              result = "File has been loaded successfully";
          }
          
          FeedDate = fname.substring(0, 4) + "-" + fname.substring(4, 6) + "-" + fname.substring(6, 8);
          seq = Integer.parseInt(fname.substring(fname.length()-1, fname.length()));
          seqold = seq-1;

          if (seq == 2) {
             ActTimeOld = GetPrevTimeStamp();
          }
          
          Statement Intst = conn.createStatement();
          int load = Intst.executeUpdate("USE " + database + " INSERT INTO tbl_opslog (file_name, acttime, mode, insert_cnt, update_cnt, error_cnt, result, feeddate, seq, acttime_new, seq_new)" +
           "VALUES ('" + fname + "', '" + ActTimeOld + "', '" + mode + "', " + insert + ", " + update + ", " + err + ", '" + result + "', '" + FeedDate + "', " + seqold + ", '" + ActTime + "', " + seq + ")");   
          if(load <= 0){
                if (bAutoRun==0 || bAutoRun==1){
                    javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")InsertLog:: Could not log file: " + fname);
                }else{
                    log.append("(MsSql: " + hostname + "/" + host + ")InsertLog:  Could not log file: " + fname,  LogFilePath, "WCALoader");                               
                }
          }else{
              if(runonload!= null){
                    if(runonload.length() > 0) dosShell.RunIt(runonload);
              }
          }
          Intst.close();
          
      }catch(Exception e){
         if (bAutoRun==0 || bAutoRun==1){
            javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")InsertLog: Error loging file: " + fname + "\t" + e.getMessage());
         }else{
            log.append("(MsSql: " + hostname + "/" + host + ")InsertLog: Error loging file: " + fname + "\t" + e.getMessage(),  LogFilePath, "WCALoader");                               
         } 
      }
      return tcount;
  }
  
 
  
  private String GetTimeStamp(){
      try{
          String TimeStamp = "";
          Statement TSst = conn.createStatement();
                ResultSet TSrs = TSst.executeQuery("USE " + database + " SELECT CONVERT(VARCHAR(20),GETDATE(),120) AS ActTime");
                if(TSrs != null){
                    TSrs.next();                    
                    if(TSrs.getRow()>0){
                        TimeStamp = TSrs.getString("ActTime").trim();
                    }else{
                        TimeStamp = "";
                    }
                }else{
                    TimeStamp = "";
                }
                TSrs.close();
                TSst.close();  
          return TimeStamp;
      }catch(Exception e){
          if (bAutoRun==0 || bAutoRun==1){
            javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")GetTimeStamp: "+ e.getMessage());
         }else{
            log.append("(MsSql: " + hostname + "/" + host + ")GetTimeStamp: " + e.getMessage(),  LogFilePath, "WCALoader");                               
         } 
         return ""; 
      }
  }
    
  private String GetPrevTimeStamp(){
      try{
          String TimeStamp = "";
          Statement TSst = conn.createStatement();
                ResultSet TSrs = TSst.executeQuery("USE wca SELECT CONVERT(VARCHAR(20),max(acttime),120) AS ActTime from tbl_opslog WHERE seq_new=1");
                if(TSrs != null){
                    TSrs.next();                    
                    if(TSrs.getRow()>0){
                        TimeStamp = TSrs.getString("ActTime").trim();
                    }else{
                        TimeStamp = "";
                    }
                }else{
                    TimeStamp = "";
                }
                TSrs.close();
                TSst.close();  
          return TimeStamp;
      }catch(Exception e){
          if (bAutoRun==0 || bAutoRun==1){
            javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")GetTimeStamp: "+ e.getMessage());
         }else{
            log.append("(MsSql: " + hostname + "/" + host + ")GetTimeStamp: " + e.getMessage(),  LogFilePath, "WCALoader");                               
         } 
         return ""; 
      }
  }
    

  private void CheckFields(String[] fldVal){
       String fld = "";
       try{          
           for(int n=0;n<ChkFld.length;n++){
               //if(!ChkFld[n].toUpperCase().startsWith("CHAR") && !ChkFld[n].toUpperCase().startsWith("VARCHAR") && !ChkFld[n].toUpperCase().startsWith("TEXT")){                   
               if(!ChkFld[n].toUpperCase().startsWith("CHAR") && !ChkFld[n].toUpperCase().startsWith("VARCHAR")){
                   if(ChkFld[n].compareToIgnoreCase("datetime") == 0 || ChkFld[n].compareToIgnoreCase("date") == 0 ){
                        if(fldVal[n].length()>0){
                           SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                           fld = fldName[n] + "--->" +fldVal[n];
                           if(sdf.parse(fldVal[n]).before(sdf.parse("19010101"))){
                               /* if (bAutoRun==0 || bAutoRun==1){
                                       javax.swing.JOptionPane.showMessageDialog(null,"(MsSql) Date changed to null: " + fldName[n] + "\n" + record);
                                }else{
                                       log.append("(MsSql)Date changed to null: " + fldName[n] + "\n" + record,  LogFilePath, "WCALoader");
                                }
                                err++;*/
                                fldVal[n] = "null";
                                //return false;
                                //return true;
                           }
                       }else{
                           fld = fldName[n] + "--->" +fldVal[n];
                           fldVal[n] = "null";
                       }
                   }else{
                       if(fldVal[n].length()<=0){
                           if(ChkFld[n].toUpperCase().startsWith("TEXT")){
                               fld = fldName[n] + "--->" +fldVal[n];
                               fldVal[n] = "No further information";
                           }else{
                                fld = fldName[n] + "--->" +fldVal[n];
                                fldVal[n] = "null";
                           }
                       }
                   }
               }
           }
       }catch(Exception e){
             if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")CheckDate: "+ e.getMessage() + "\n "+ fld + "\n" + record );
             }else{
                log.append("(MsSql: " + hostname + "/" + host + ")CheckDate: " + e.getMessage() + "\n" + fld + "\n" + record,  LogFilePath, "LogFile");                               
             } 
           err++;
       }
            
   }
   
   private String GetMaxFeedDate(){
       try{
           String rtn = "";
           Statement mst = conn.createStatement();
           ResultSet mrs = mst.executeQuery("select max(feeddate) as mdt from smf4.dbo.tbl_opslog");
           if(mrs != null){
               while(mrs.next()){
                   rtn = mrs.getString("mdt");
               }
           }
           mrs.close();
           mst.close();
           return rtn;
       }catch(Exception e){
           if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql: " + hostname + "/" + host + ")GetMaxFeedDate: "+ e.getMessage());
             }else{
                log.append("(MsSql: " + hostname + "/" + host + ")GetMaxFeedDate: " + e.getMessage(),  LogFilePath, "LogFile");                               
             } 
           err++;
           return "";
       }
   }
    
    public void Closeconn(){
        conn = null;
    }

}
