/*
 * scheduler.java
 * Created on 04 September 2008, 14:19
 * @author  h.patel
 */

package wcaloader;

import com.edi.common.Logging;
import com.gc.systray.SystemTrayIconListener;
import com.gc.systray.SystemTrayIconManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;


public class scheduler extends javax.swing.JFrame implements SystemTrayIconListener{
    
    private static String sAppName = "WCALoader";
    private static String sVerMajor = "1";
    private static String sVerMinor = "0";
    private static String sVerRevision = "0";    
    private static String sVersion = sVerMajor+"."+sVerMinor+"."+sVerRevision;
    private static String sBuildDate = "27 Oct 2008";
    private int RunTimes[] = {0,15,30,45};
    private boolean bIsRunning = false;
    
    private Logging		log = new Logging("Automation");   
    private javax.swing.DefaultListModel model = new javax.swing.DefaultListModel();
    
    private String LogFile = "";
    private SystemTrayIconManager mgr;
    private JPopupMenu jPopupMenu1;
    private JMenuItem mnuWait;
    private JMenuItem mnuDoing;        
    private JMenuItem mnuMessage;    
    private JMenu jMenu;
    private int popupnumber =0;         
    private String[] Margs = null;
    private String AppPath = System.getProperty("user.dir");
    
    /** Creates new form scheduler */
    public scheduler(String args[]){
        try{
            try {
                javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e){
            //System.out.println(e);
            }  
            initComponents();            

            if(args.length<=0){
                //load.writelogfile(System.getProperty("user.dir") + "\\" + DtToday +".log","No Parameter found");
                System.out.println("No Parameter found");
                System.exit(0);
            }
            
            //Get parameter
            Margs = args;            

            //Get Logfile path
            for(int f=0;f<args.length;f++){
                //Get Logfile path
                if(args[f].equalsIgnoreCase("-l")){
                        LogFile = args[f+1];
                 }
                //Get config file path
                 if(args[f].equalsIgnoreCase("-cf")){
                        AppPath = args[f+1];
                 }
            }
            
            createpopup();
            lstMessages = new javax.swing.JList(model);
            startLoading(args);
        }catch(Exception e){
            log.append("scheduler:" + e.getMessage(), LogFile, "WCALoader");
        }
    }
    
    public void mouseClickedLeftButton(Point pos, SystemTrayIconManager source) {
        //System.out.println("left click at (" + pos.getX() + ", " + pos.getY() + ")");
    }
    public void mouseClickedRightButton(Point pos, SystemTrayIconManager ssource) {
        //System.out.println("right click at (" + pos.getX() + ", " + pos.getY() + ")");
    }
    public void mouseLeftDoubleClicked(Point pos, SystemTrayIconManager source) {
        if (this.isVisible())
            setVisible(false);
        else
            setVisible(true);
    }
    public void mouseRightDoubleClicked(Point pos, SystemTrayIconManager source) {
        //System.out.println("right double click at (" + pos.getX() + ", " + pos.getY() + ")");
    }
    
   private void startLoading(String args[]) {
        try{
            createTrayIcon();
            boolean bContinue=true;
            while (bContinue){	    
                java.util.Date dCheckDate = new java.util.Date();
                lblStatusText.setText(dCheckDate.toString());
                if (dCheckDate.getSeconds()==0){		
                    int currentMinute = dCheckDate.getMinutes();		
                    for (int tc=0; tc<RunTimes.length;tc++){
                        if (dCheckDate.getMinutes() == RunTimes[tc]){                            
                            showpopup(sAppName + " - " + sVersion, "Running checks.", "Please Wait...");   
                            lblStatusText.setText("Executing pending tasks, Please wait.....");
                            if ( bIsRunning==true){
                                System.out.println("\n---> Unable to run check, check is already running.\n");
                                System.gc();
                                return;            
                            }
                            if (model.getSize()>50){
                                model.clear();
                                ScPan.setViewportView(lstMessages);
                            }
                            bIsRunning=true;                            
                            //System.out.println("Load");
                            new loader(args);
                            bIsRunning=false;
                            model.add(0, "Last Checked @ " + (new java.util.Date().toString()));
                            ScPan.setViewportView(lstMessages);
                            lstMessages.setSelectedIndex(0);
                            hidepopup();
                        }
                        lblStatusText.setText(dCheckDate.toString());
                    }		
                }	    	    
                waitFor(1);	    
            } // End While            
            System.gc();
        }catch ( Exception e ) {
	    //System.out.println(e.getMessage());
            log.append("startLoading:" + e.getMessage(), LogFile, "WCALoader");            
	}
    }   
   
   private void createpopup(){
        try{
            jPopupMenu1 = new JPopupMenu();

            mnuWait= new JMenuItem();
            mnuDoing = new JMenuItem();
            mnuMessage = new JMenuItem();        

            jMenu = new JMenu();

            mnuWait.setEnabled(false);
            mnuDoing.setEnabled(true);
            mnuMessage.setEnabled(true);

            mnuWait.setText("WCALoader - Version 1.0.0");
            mnuDoing.setText("Starting System");
            mnuMessage.setText("Please Wait...");        
            jPopupMenu1.add(mnuWait);          
            jPopupMenu1.add(mnuDoing);          

            java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();                        
            jPopupMenu1.setLocation(screenSize.width-jPopupMenu1.getWidth(), screenSize.height-jPopupMenu1.getHeight());
            jPopupMenu1.show(null,screenSize.width-jPopupMenu1.getWidth(), screenSize.height-jPopupMenu1.getHeight());                
            jPopupMenu1.setVisible(false);
        }catch ( Exception e ) {
	    //System.out.println(e.getMessage());
            log.append("createpopup:" + e.getMessage(), LogFile, "WCALoader");
	}
    }    
    
    private void showpopup(String sTitle, String msg1, String msg2){
        try{
            // Set the text for each menu item
            mnuWait.setText(sTitle);
            mnuDoing.setText(msg1);
            mnuMessage.setText(msg2);

            // position and display popup
            java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();                        
            jPopupMenu1.getWidth();        
            jPopupMenu1.show(null,screenSize.width-jPopupMenu1.getWidth(), screenSize.height-jPopupMenu1.getHeight());                
            jPopupMenu1.paint(jPopupMenu1.getGraphics()); 
            waitFor(2);
        }catch ( Exception e ) {
	    //System.out.println(e.getMessage());
            log.append("showpopup:" + e.getMessage(), LogFile, "WCALoader");
	}
    }   
    
    private void hidepopup(){
        try{
            jPopupMenu1.setVisible(false);        
        }catch ( Exception e ) {
	    //System.out.println(e.getMessage());
            log.append("hidepopup:" + e.getMessage(), LogFile, "WCALoader");
	}
    }   
   
   private void createTrayIcon(){
        try{
            JPopupMenu jPopupMenu1 = new JPopupMenu();
                JMenuItem mnuExit = new JMenuItem("Exit");
                //JMenuItem mnuAbout = new JMenuItem("About "+sAppName);
                JMenuItem mnuShowHide = new JMenuItem("Show/Hide");        
            JMenu jMenu = new JMenu();                
            jPopupMenu1.add(mnuShowHide);        
            //jPopupMenu1.add(mnuAbout);
            jPopupMenu1.add(mnuExit);

            if (!SystemTrayIconManager.initializeSystemDependent()) {
                javax.swing.JOptionPane.showMessageDialog(null,"unable to load required DLL for Tray Icon, Please contact your System Administrator");
                System.out.println("DLL error");
                System.exit(1);
            }
            int quick = SystemTrayIconManager.loadImage(AppPath + "\\scheduler.ico");
            if (quick == -1) {            
                javax.swing.JOptionPane.showMessageDialog(null,"unable to load required ICON for system tray, Please contact your System Administrator");
                System.out.println("image icon.ico error");
                System.exit(1);
            }

            mgr = new SystemTrayIconManager(quick, "WCALoader "+sVersion);

            // Add the System Tray Listener
            mgr.addSystemTrayIconListener(this);

            // Add Listener for Exit Menu
            mnuExit.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {                
                    mgr.setVisible(false);
                    System.exit(0);
                }
            });
            // Add Listener for About
            /*mnuAbout.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    splash.setText("About");
                    splash.setDelay(15);
                    }	    
            });*/

            // Add Listener for Show/Hide menu
            mnuShowHide.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if ( isVisible() )
                        hide();
                    else{
                        show();
                        //removeTrayIcon();
                    }
                };
            });

            // Set Mouse Clicks.
            mgr.setRightClickView(jPopupMenu1);
            //mgr.setLeftClickView(new javax.swing.JLabel("Text test"));
            //mgr.setLeftClickView(new JFrame("Frame Title"));	

            // make the icon visible.
            mgr.setVisible(true);
        }catch ( Exception e ) {
	    //System.out.println(e.getMessage());
            log.append("createTrayIcon:" + e.getMessage(), LogFile, "WCALoader");
	}
        
 }
   
 private void removeTrayIcon(){
        try{
            mgr.setVisible(false);
            mgr.removeSystemTrayIconListener(this);
        }catch ( Exception e ) {
	    //System.out.println(e.getMessage());
            log.append("removeTrayIcon:" + e.getMessage(), LogFile, "WCALoader");
	}
 }  
   
 private boolean waitFor(int Secs){
	try{
	    Thread.sleep(Secs*1000);
            return true;
	} catch ( Exception e){	    
            log.append("waitFor:" + e.getMessage(), LogFile, "WCALoader");
            return false;
	}	
  }
 
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cmdLog = new javax.swing.JButton();
        cmdClear = new javax.swing.JButton();
        ScPan = new javax.swing.JScrollPane();
        lstMessages = new javax.swing.JList();
        jPanel2 = new javax.swing.JPanel();
        lblStatusText = new javax.swing.JLabel();
        lblPoll = new javax.swing.JLabel();

        setTitle("WCA Loader");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setMaximumSize(new java.awt.Dimension(160, 32767));
        jPanel1.setMinimumSize(new java.awt.Dimension(160, 50));
        jPanel1.setPreferredSize(new java.awt.Dimension(160, 50));

        cmdLog.setText("Log");
        cmdLog.setPreferredSize(new java.awt.Dimension(120, 23));
        cmdLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdLogActionPerformed(evt);
            }
        });
        jPanel1.add(cmdLog);

        cmdClear.setText("Clear");
        cmdClear.setMaximumSize(new java.awt.Dimension(90, 23));
        cmdClear.setMinimumSize(new java.awt.Dimension(90, 23));
        cmdClear.setPreferredSize(new java.awt.Dimension(120, 23));
        cmdClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdClearActionPerformed(evt);
            }
        });
        jPanel1.add(cmdClear);

        getContentPane().add(jPanel1, java.awt.BorderLayout.EAST);

        ScPan.setViewportView(lstMessages);

        getContentPane().add(ScPan, java.awt.BorderLayout.CENTER);

        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.LINE_AXIS));

        lblStatusText.setText("StatusMessage");
        lblStatusText.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lblStatusText.setMaximumSize(new java.awt.Dimension(1280, 19));
        jPanel2.add(lblStatusText);

        lblPoll.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPoll.setText("-------");
        lblPoll.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lblPoll.setMaximumSize(new java.awt.Dimension(100, 19));
        lblPoll.setMinimumSize(new java.awt.Dimension(100, 19));
        lblPoll.setPreferredSize(new java.awt.Dimension(100, 19));
        jPanel2.add(lblPoll);

        getContentPane().add(jPanel2, java.awt.BorderLayout.SOUTH);

        getAccessibleContext().setAccessibleName("WCA Loader");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdClearActionPerformed
        try{
            model.clear();
            ScPan.setViewportView(lstMessages);
        } catch (Exception e){
            log.append("cmdClearActionPerformed:" + e.getMessage(), LogFile, "WCALoader");
        }
    }//GEN-LAST:event_cmdClearActionPerformed

    private void cmdLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdLogActionPerformed
        /*try{
            File lf = new File( LogFile + cm.GetCurnDate().substring(2,8) + "_WCALoader" + ".log");
            if(lf.exists()){
                Runtime.getRuntime().exec("notepad " + LogFile + cm.GetCurnDate().substring(2,8) + "_WCALoader" + ".log");
            }else{
                JOptionPane.showMessageDialog(null,"LogFile " + LogFile + cm.GetCurnDate().substring(2,8) + "_WCALoader" + ".log does not exists" );
            }
        } catch (Exception e){
                JOptionPane.showMessageDialog(null,"LogFile: " + e.getMessage());
        }*/
    }//GEN-LAST:event_cmdLogActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new scheduler(args);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JScrollPane ScPan;
    public javax.swing.JButton cmdClear;
    private javax.swing.JButton cmdLog;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JLabel lblPoll;
    public javax.swing.JLabel lblStatusText;
    public javax.swing.JList lstMessages;
    // End of variables declaration//GEN-END:variables
    
}
