/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */

package wcaloader;

import com.edi.common.Logging;
import com.edi.common.MsSqlConnection.MsSqlConnection;
import com.edi.common.dates;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class MsSql_Realign {

    private Logging log = new Logging("Automation");
    private MsSqlConnection mssql = null;
    private Connection conn = null;
    private String user = null;
    private String pass = null;
    private String database = null;
    private String host = null;
    private int bAutoRun = 0;
    private String LogFilePath = "";
    private String record = "";
    private int insert = 0;
    private int err = 0;
    private String[] fldName = null;
    private String[] ChkFld = null;
    private String LTbl = "";
    public boolean TblExist = false;
    public boolean isTblRecDel = false;
    private String LogFile = "";
    private String logsrv = "";
    
    public MsSql_Realign(){
        
    }
    
    public boolean initialization(String Tuser, String Tpass, String Tdatabase, String Thost, int Auto, String LFilePath, String lsrv){
        user = Tuser;
        pass = Tpass;
        database = Tdatabase;
        host = Thost;
        logsrv = lsrv;
        if(OpenDBConnection()){                        
            bAutoRun = Auto;
            LogFilePath = LFilePath;            
            return true;
        }else{
            return false;
        }
    }
    
    private boolean OpenDBConnection(){
        try{
            mssql = new MsSqlConnection(user,pass,database,host);
            conn = mssql.getConnection();
            return true;
       }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)OpenDBConnection: " + e.getMessage());
            }else{
                log.append("(MsSql)OpenDBConnection: " + e.getMessage(),  LogFilePath, logsrv + "_WCALoader_Realign");                               
            }
            return false;
       }
    }
     
    public void MsLoading(String row, int count){
          try{
              record = row;
              String[] Sfld = row.split("\\|");
              if(Sfld != null){
                  for(int c=0;c<Sfld.length;c++){
                      if(Sfld[c]!=null){
                          Sfld[c] = Sfld[c].trim();
                          if(c == 1)Sfld[c]= Sfld[c].toLowerCase();
                      }else{
                          Sfld[c] = "";
                      }
                  }
                  
                  LogFile = Sfld[1];
                  if(LTbl.equals(Sfld[1])){ // Check for table if it is the same 
                      if(TblExist){
                          String[] fldValue = new String[Sfld.length - 3];
                          int cf = 0;
                          for(int c=2;c < Sfld.length - 1;c++){
                              fldValue[cf] = Sfld[c];
                              cf++;
                          }
                          
                          //load record
                          CheckFields(fldValue);
                          InsertRec(Sfld[1], fldName, fldValue);
                                       
                      }else{
                          if (bAutoRun==0 || bAutoRun==1){
                                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)Table not exists: " + Sfld[1] + "\n" + row);
                          }else{
                                log.append("(MsSql)Table not exists: " + Sfld[1] + "\n" + row,  LogFilePath, logsrv + "_WCALoader_Realign");                               
                          }
                          err++;
                      }

                  }else if(count == 1){ // Check for table if it is not the same
                          LTbl =  Sfld[1];
                          Statement Tst = conn.createStatement();
                          ResultSet Trs = Tst.executeQuery(" USE wca " +
                                                           " SELECT * FROM INFORMATION_SCHEMA.TABLES " +
                                                           " WHERE TABLE_TYPE = 'BASE TABLE' " +
                                                           " AND TABLE_NAME = '" + Sfld[1] + "'");
                          Trs.next();
                          if(Trs.getRow() > 0){
                              if(DelRecords(Sfld[1])){
                                  isTblRecDel = true;
                                  TblExist = true;
                                  //Get columns
                                   Statement Cst = conn.createStatement();
                                   ResultSet Crs = Cst.executeQuery("USE wca " +
                                                                    " SELECT * FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '" + Sfld[1] + "'" +
                                                                    " ORDER BY ORDINAL_POSITION" );

                                   fldName = new String[Sfld.length - 3];
                                   ChkFld = new String[Sfld.length - 3];
                                   String[] fldValue = new String[Sfld.length - 3];
                                   if(Sfld.length == (GetColCnt(Sfld[1]) - 2)){

                                       //Get Fields
                                       int pos = 0;
                                       int cnt =0;
                                       while(Crs.next()){
                                           if((pos = Crs.getInt("ORDINAL_POSITION")) > 5){
                                               fldName[cnt] = Crs.getString("COLUMN_NAME");
                                               ChkFld[cnt] = Crs.getString("DATA_TYPE");
                                               fldValue[cnt] = Sfld[pos - 4];
                                               cnt++;
                                           }
                                       }

                                   }//length of the fields
                                   Crs.close();
                                   Cst.close();

                                   //load record
                                   CheckFields(fldValue);
                                   InsertRec(Sfld[1], fldName, fldValue);
                               }else{
                                  log.append("(MsSql)Could not delete records: " + Sfld[1] ,  LogFilePath, logsrv + "_WCALoader_Realign");
                                  isTblRecDel = false;
                               }

                          }else{
                              if (bAutoRun==0 || bAutoRun==1){
                                    javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)Table not exists: " + Sfld[1] + "\n" + row);
                              }else{
                                    log.append("(MsSql)Table not exists: " + Sfld[1] + "\n" + row,  LogFilePath, logsrv + "_WCALoader_Realign");                               
                              }      
                              err++;
                              TblExist = false;
                          }//check table exists
                          Trs.close();
                          Tst.close();
                      
                  }else{
                      if (bAutoRun==0 || bAutoRun==1){
                            javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)Different Table: " + Sfld[1] + "\n" + row);
                      }else{
                            log.append("(MsSql)Different Table: " + Sfld[1] + "\n" + row,  LogFilePath, logsrv + "_WCALoader_Realign");
                      }  
                  }
              }
          }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)MsLoading: " + e.getMessage() + "\n" + row);
            }else{
                log.append("(MsSql)MsLoading: " + e.getMessage()+ "\n" + row,  LogFilePath, logsrv + "_WCALoader_Realign");
            }   
            err++;
          }
    }
    
    private void InsertRec(String tbl, String[] FName, String[] FVal){
        String sqlcmd = "USE wca INSERT INTO " + tbl + "( acttime,actflag,";
        try{
            int GCAction = -1, GCDate = -1, GCTime = -1;            
            for(int f1=0;f1<FName.length;f1++){
               if(FName[f1].equalsIgnoreCase("GCAction"))  GCAction = f1;
               if(FName[f1].equalsIgnoreCase("GCDate"))    GCDate = f1;
               if(FName[f1].equalsIgnoreCase("GCTime"))    GCTime = f1;
               sqlcmd += FName[f1] + ",";
            }
            
            String ActTime = "";
            try{
                ActTime = FVal[GCDate].substring(0, 4) + "-" +  FVal[GCDate].substring(4, 6) + "-" +  FVal[GCDate].substring(6, 8) + " " + FVal[GCTime];
            }catch(Exception e){
                if (bAutoRun==0 || bAutoRun==1){
                    javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)GCA control fields empty: " + e.getMessage() + "\n" + record);
                }else{
                    log.append("(MsSql)GCA control fields empty: " + e.getMessage()+ "\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");
                    ActTime = new dates().getJapDate(new java.util.Date());
                    ActTime = ActTime.substring(0,10) + " 12:00:00";
                }
            }
            //Checking Actflag 
            if(FVal[GCAction].equalsIgnoreCase("A"))   FVal[GCAction] = "I";
            
            sqlcmd =sqlcmd.substring(0, sqlcmd.length() - 1) + ") VALUES ( '" + ActTime + "', '" + FVal[GCAction] + "'," ;
            for(int f1=0;f1<FVal.length;f1++){
                FVal[f1] = FVal[f1].replace("'", "`");
                FVal[f1] = FVal[f1].replace("\"", "`");
                FVal[f1] = FVal[f1].replace("\\", "`");
                FVal[f1] = FVal[f1].replace("\t", " ");
                sqlcmd += "'" + FVal[f1] + "',";
            }                
            sqlcmd = sqlcmd.substring(0, sqlcmd.length() - 1) + ")";
            sqlcmd = sqlcmd.replace("'null'", "null");
            Statement Insertst = conn.createStatement();
            int update_result = Insertst.executeUpdate(sqlcmd);
            Insertst.close();
            if(update_result<=0){
                err++;
                log.append("Could not load\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");
            }else{
                insert++;
            }    
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)InsertRec: " + e.getMessage() + "\n" + record);
            }else{
                log.append("(MsSql)InsertRec: " + e.getMessage()+ "\n" + sqlcmd + "\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");
            }   
            err++;
        }
    }
    
 
   private int GetColCnt(String tbl){
       try{
           int cnt = 0;
           Statement Colst = conn.createStatement();
           ResultSet Colrs = Colst.executeQuery("USE wca " +
                                                " SELECT COUNT(*) AS cnt FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '" + tbl + "'");
           Colrs.next();  
           cnt = Colrs.getInt("cnt");
           Colst.close();
           Colrs.close();
           return cnt;
       }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)GetColCnt: " + e.getMessage());
            }else{
                log.append("(MsSql)GetColCnt: " + e.getMessage(),  LogFilePath, logsrv + "_WCALoader_Realign");                               
            }
            return 0;
       }
   }
   
  
  
  private String GetTimeStamp(){
      try{
          String TimeStamp = "";
          Statement TSst = conn.createStatement();
                ResultSet TSrs = TSst.executeQuery("USE wca SELECT CONVERT(VARCHAR(20),GETDATE(),120) AS ActTime");
                if(TSrs != null){
                    TSrs.next();                    
                    if(TSrs.getRow()>0){
                        TimeStamp = TSrs.getString("ActTime").trim();
                    }else{
                        TimeStamp = "";
                    }
                }else{
                    TimeStamp = "";
                }
                TSrs.close();
                TSst.close();  
          return TimeStamp;
      }catch(Exception e){
          if (bAutoRun==0 || bAutoRun==1){
            javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)GetTimeStamp: "+ e.getMessage());
         }else{
            log.append("(MsSql)GetTimeStamp: " + e.getMessage(),  LogFilePath, logsrv + "_WCALoader_Realign");                               
         } 
         return ""; 
      }
  }
    
  private void CheckFields(String[] fldVal){
       String fld = "";
       try{          
           for(int n=0;n<ChkFld.length;n++){
               if(!ChkFld[n].toUpperCase().startsWith("CHAR") && !ChkFld[n].toUpperCase().startsWith("VARCHAR")){
                   if(ChkFld[n].compareToIgnoreCase("datetime") == 0 || ChkFld[n].compareToIgnoreCase("date") == 0 ){
                        if(fldVal[n].length()>0){
                           SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                           fld = fldName[n] + "--->" +fldVal[n];
                           if(sdf.parse(fldVal[n]).before(sdf.parse("19010101"))){
                                /*if (bAutoRun==0 || bAutoRun==1){
                                       javax.swing.JOptionPane.showMessageDialog(null,"(MsSql) Date changed to null: " + fldName[n] + "\n" + record);
                                }else{
                                       log.append("(MsSql)Date changed to null: " + fldName[n] + "\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");
                                }
                                err++;*/
                                fldVal[n] = "null";
                                //return false;
                                //return true;
                           }
                       }else{
                           fld = fldName[n] + "--->" +fldVal[n];
                           fldVal[n] = "null";
                       }
                   }else{
                       if(fldVal[n].length()<=0){
                           if(ChkFld[n].toUpperCase().startsWith("TEXT")){
                               fld = fldName[n] + "--->" +fldVal[n];
                               fldVal[n] = "No further information";
                           }else{
                                fld = fldName[n] + "--->" +fldVal[n];
                                fldVal[n] = "null";
                           }
                       }
                   }
               }
           }
       }catch(Exception e){
             if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)CheckDate: "+ e.getMessage() + "\n "+ fld + "\n" + record );
             }else{
                log.append("(MsSql)CheckDate: " + e.getMessage() + "\n" + fld + "\n" + record,  LogFilePath, logsrv + "_WCALoader_Realign");                               
             } 
           err++;
       }
            
   }  

   private boolean DelRecords(String tbl){
       try{
           
           Statement Dst = conn.createStatement();
           Dst.executeUpdate("USE WCA IF (SELECT COUNT(*) FROM " + tbl + ") > 0" +
                               " begin " +
                               " DELETE FROM " + tbl +
                               " end");
           return true;
       }catch(Exception e){
             if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"(MsSql)DelRecords: "+ e.getMessage());
             }else{
                log.append("(MsSql)DelRecords: " + e.getMessage(),  LogFilePath, logsrv + "_WCALoader_Realign");                               
             }              
             err++;
             return false;
       }
   }
  
    public void Closeconn(){
        conn = null;
    }

}
