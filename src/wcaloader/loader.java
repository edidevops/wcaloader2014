/*
 * loader.java
 * Created on 16 October 2008, 09:08
 * @author  h.patel
 */
package wcaloader;

import com.edi.common.Logging;
import com.edi.gmailer;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class loader extends javax.swing.JFrame {

    private Logging log = new Logging("Automation");
    private String AppPath = System.getProperty("user.dir");
    private String ConfigFile = "";
    private String LogFilePath = AppPath + "\\";
    private int bAutoRun = 0;
    private String InputPath = "";
    private String ArchivePath = "";
    private String driver = null;
    private String user = null;
    private String pass = null;
    private String database = "wca";
    private String host = null;
    private String cmdSrv = "";
    private javax.swing.JList lstLoadList;
    private String FileName = "";
    private ArrayList TabList = new ArrayList();
    private String Tblname = "";
    private boolean Tbl = false;
    public int RecCnt = 0;
    private int cnt = 0;
    private boolean LoadTbl = false;
    private boolean isAlert = false;
    private String ErrCode = "0";
    private String runonload = "";
    private String server = "";
    private boolean continuous = false;
    private List<Database> dbs = new ArrayList();

    /**
     * Creates new form loader
     */
    public loader(String[] args) {

        // Set Look and Feel to Native style.
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            //System.out.println(e);
        }

        initComponents();

        try {

            if (args.length <= 0) {
                System.out.println("No Parameter found");
                exitForm(Integer.parseInt(ErrCode));
            }
            String TabLst = AppPath + "\\tablist.txt";
            for (int f = 0; f < args.length; f++) {
                if (args[f].equalsIgnoreCase("-cfgfile")) {
                    ConfigFile = args[f + 1];
                } else if (args[f].equalsIgnoreCase("-m")) {
                    bAutoRun = 1;
                } else if (args[f].equalsIgnoreCase("-a")) {
                    btLoad.setEnabled(false);
                    bAutoRun = 2;
                } else if (args[f].equalsIgnoreCase("-s")) {
                    //Getting the connection details                    
                    server = args[f + 1];
                } else if (args[f].equalsIgnoreCase("-i")) {
                    InputPath = args[f + 1];
                } else if (args[f].equalsIgnoreCase("-l")) {
                    LogFilePath = args[f + 1];
                } else if (args[f].equalsIgnoreCase("-r")) {
                    ArchivePath = args[f + 1];
                } else if (args[f].equalsIgnoreCase("-tablst")) {
                    TabLst = args[f + 1];
                    GetTableList(TabLst);
                    Tbl = true;
                } else if (args[f].equalsIgnoreCase("-alert")) {
                    isAlert = true;
                } else if (args[f].equalsIgnoreCase("-errcd")) {
                    ErrCode = args[f + 1];
                } else if (args[f].equalsIgnoreCase("-runonload")) {
                    runonload = args[f + 1];
                } else if (args[f].equalsIgnoreCase("-db")) {
                    database = args[f + 1];
                } else if (args[f].equalsIgnoreCase("-continuous")) {
                    continuous = true;
                } else if (args[f].equalsIgnoreCase("-sdb")) {
                    String sdb = args[f + 1];
                    if (sdb.contains(",")) {
                        String[] values = args[f + 1].split(",");
                        for (int i = 0; i < values.length; i++) {
                            String[] dbValues = values[i].split("-");
                            Database db = new Database();
                            db.setDbName(dbValues[0]);
                            db.setTblName(dbValues[1]);
                            dbs.add(db);
                        }
                    } else {
                        String[] dbValues = args[f + 1].split("-");
                        Database db = new Database();
                        db.setDbName(dbValues[0]);
                        db.setTblName(dbValues[1]);
                        dbs.add(db);
                    }
                }
            }
            getConnDetail(server);
            setVisible(true);

            if (bAutoRun == 2) {
                btLoadActionPerformed(null);
                exitForm(0);
            }
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "Exe Parameter Failed: " + e, "Invalid Exe Paramenter", javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            } else {
                log.append("Exe Parameter Failed: " + e.getMessage(), LogFilePath, "WCALoader");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new javax.swing.JPanel();
        ProgressBar = new javax.swing.JProgressBar();
        btLoad = new javax.swing.JButton();
        ScPan = new javax.swing.JScrollPane();
        lblStatus = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("WCA Loader");

        btLoad.setText("Load");
        btLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLoadActionPerformed(evt);
            }
        });

        lblStatus.setText("Status");

        org.jdesktop.layout.GroupLayout PanelLayout = new org.jdesktop.layout.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(PanelLayout.createSequentialGroup()
                .add(PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, lblStatus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, PanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, ScPan)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, PanelLayout.createSequentialGroup()
                                .add(ProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 341, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btLoad)))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(PanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(btLoad, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(ProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(13, 13, 13)
                .add(ScPan, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(lblStatus, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(Panel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(Panel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLoadActionPerformed
        try {
            int repeater = 0;
            while (repeater == 0) {
                String Lfile = "";
                MsSql Ms = new MsSql();
                MySql My = new MySql();
                if (bAutoRun == 0 || bAutoRun == 1) {
                    btLoad.setEnabled(false);
                }

                //Reset Process Message
                lblStatus.setText("Opening file please wait...... ");
                Panel.paintImmediately(0, 0, Panel.getWidth(), Panel.getHeight());

                if (driver.compareToIgnoreCase("mysql") == 0) {
                    Ms = null;
                    if (My.initialization(user, pass, database, host, bAutoRun, LogFilePath, runonload, server)) {
                        Lfile = My.GetLogFileName();
                    } else {
                        My = null;
                        if (bAutoRun == 0 || bAutoRun == 1) {
                            javax.swing.JOptionPane.showMessageDialog(null, "Could not connect to server: " + driver + "\t" + host);
                        } else {
                            log.append("Could not connect to server: " + driver + "\t" + host, LogFilePath, "WCALoader");
                            exitForm(Integer.parseInt(ErrCode));
                        }
                    }
                } else if (driver.compareToIgnoreCase("mssql") == 0) {
                    My = null;
                    if (Ms.initialization(user, pass, database, host, bAutoRun, LogFilePath, runonload, server)) {
                        Lfile = Ms.GetLogFileName();
                    } else {
                        Ms = null;
                        if (bAutoRun == 0 || bAutoRun == 1) {
                            javax.swing.JOptionPane.showMessageDialog(null, "Could not connect to server: " + driver + "\t" + host);
                        } else {
                            log.append("Could not connect to server: " + driver + "\t" + host, LogFilePath, "WCALoader");
                            exitForm(Integer.parseInt(ErrCode));
                        }
                    }
                }

                // Loop though files and load as needed
                // GenericLocator fileLocator = new GenericLocator(InputPath,Lfile,"");
                ArrayList fileToLoad = new ArrayList();
                fileToLoad.add(Lfile);
                //Collections.sort(fileToLoad);
                ShowFileList(fileToLoad);
                setNextFile(0);
                if (bAutoRun != 2) {
                    ProgressBar.setMinimum(0);
                    ProgressBar.setString(null);
                    ProgressBar.setStringPainted(true);
                    ProgressBar.setValue(0);
                }
                Panel.paintImmediately(0, 0, Panel.getWidth(), Panel.getHeight());
                if (UnpackZipFile(InputPath + Lfile, AppPath + "\\UNPACKED\\")) {
                    FileName = AppPath + "\\UNPACKED\\" + Lfile.replace(".z", ".0");
                } else {
                    if (bAutoRun == 0 || bAutoRun == 1) {
                        javax.swing.JOptionPane.showMessageDialog(null, "Could not load the file: " + Lfile);
                    } else {
                        repeater = 1;

                        log.append("Could not load the file: " + Lfile, LogFilePath, "WCALoader");
                        if (isAlert) {
                            SendEmailAlert("ops@exchange-data.com;support@exchange-data.com", "WCA " + host + " load failure", "ALERT\n\nCOULD NOT LOAD\n" + InputPath + Lfile + "\nCHECK THIS FILE!");
                        }
                        exitForm(Integer.parseInt(ErrCode));
                    }
                }

                String str = null;
                int totalcnt = 0;
                File sfile = new File(FileName);
                if (sfile.isFile()) {
                    GetRecordCount(FileName);
                    ProgressBar.setMaximum(RecCnt);
                    cnt = 1;
                    ProgressBar.setMinimum(0);
                    ProgressBar.setString(null);
                    ProgressBar.setStringPainted(true);
                    ProgressBar.setValue(0);
                    lblStatus.setText("Loading File " + FileName);
                    if (bAutoRun != 2) {
                        Panel.paintImmediately(0, 0, Panel.getWidth(), Panel.getHeight());
                    }
                    BufferedReader in = new BufferedReader(new FileReader(sfile));
                    in.readLine();
                    while ((str = in.readLine()) != null) {
                        if (!str.toUpperCase().contains("END OF WCA EXPORT FILE")) {
                            if (Tbl) {
                                if (str.toUpperCase().startsWith(Tblname) && cnt > 1) {
                                    tblExistsInDB(str, My, Ms);
                                    if (LoadTbl) {
                                        if (driver.compareToIgnoreCase("mysql") == 0) {
                                            My.MyLoading(str);
                                        } else if (driver.compareToIgnoreCase("mssql") == 0) {
                                            Ms.MsLoading(str);
                                        }
                                    }
                                } else {
                                    if (ChkTblLoad(str)) {
                                        tblExistsInDB(str, My, Ms);
                                        if (driver.compareToIgnoreCase("mysql") == 0) {
                                            My.MyLoading(str);
                                        } else if (driver.compareToIgnoreCase("mssql") == 0) {
                                            Ms.MsLoading(str);
                                        }
                                    }
                                }
                            } else {
                                tblExistsInDB(str, My, Ms);
                                if (driver.compareToIgnoreCase("mysql") == 0) {
                                    My.MyLoading(str);
                                } else if (driver.compareToIgnoreCase("mssql") == 0) {
                                    Ms.MsLoading(str);
                                }
                            }
                        }
                        if ((cnt % 10) == 0) {
                            ProgressBar.setValue(cnt);
                            lblStatus.setText("Processing Record " + ProgressBar.getValue() + " of " + ProgressBar.getMaximum() + ".");
                            if (bAutoRun != 2) {
                                Panel.paintImmediately(0, 0, Panel.getWidth(), Panel.getHeight());
                            }
                        }
                        cnt++;
                    }
                    in.close();
                    if (driver.compareToIgnoreCase("mysql") == 0) {
                        totalcnt = My.InsertLog("Daily", Lfile);
                        My.Closeconn();
                    } else if (driver.compareToIgnoreCase("mssql") == 0) {
                        totalcnt = Ms.InsertLog("Daily", Lfile);
                        Ms.Closeconn();
                    }
                }
                sfile = null;
                if (totalcnt < (RecCnt - 2)) {
                    SendEmailAlert("prodman@exchange-data.com;h.patel@exchange-data.com", "WCA " + host + " load failure", "ALERT\n\nCOULD NOT LOAD FILE PROPERLY\n" + InputPath + Lfile + "\nCROSS CHECK FILE COUNT WITH LOG TABLE!");
                }
                DeleteFile(FileName);
                DeleteFiles(AppPath + "\\UNPACKED\\");

                if (continuous == false) {
                    break;
                }
            }
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "btLoadActionPerformed: " + e.getMessage());
            } else {
                log.append("btLoadActionPerformed: " + e.getMessage(), LogFilePath, "WCALoader");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
}//GEN-LAST:event_btLoadActionPerformed

    private void getConnDetail(String svr) {

        String[] tvalues = null;
        File sourcefile = new File(ConfigFile);
        try {
            if (sourcefile.exists()) {
                BufferedReader in = new BufferedReader(new FileReader(sourcefile));
                String str = null;
                while ((str = in.readLine()) != null) {
                    //if(str.indexOf(svr)>=0)
                    tvalues = str.split("\t");
                    if (tvalues[0].compareToIgnoreCase(svr) == 0) {
                        driver = tvalues[1];
                        user = tvalues[2];
                        pass = tvalues[3];
                        host = tvalues[4];
                    }
                }
                in.close();
            } else {
                if (bAutoRun == 0 || bAutoRun == 1) {
                    javax.swing.JOptionPane.showMessageDialog(null, "Could not Found ConfigFile(" + ConfigFile + ")");
                } else {
                    log.append("Could not Found ConfigFile(" + ConfigFile + ")", LogFilePath, "WCALoader");
                }
            }
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "getConnDetail: " + e.getMessage());
            } else {
                log.append("getConnDetail: " + e.getMessage(), LogFilePath, "WCALoader");
            }
        }
    }

    private void ShowFileList(ArrayList filestoload) {

        // Create a list that allows adds and removes
        javax.swing.DefaultListModel model = new javax.swing.DefaultListModel();
        lstLoadList = new javax.swing.JList(model);

        // Initialize the list with items
        for (int i = 0; i < filestoload.size(); i++) {
            model.add(i, filestoload.get(i));
        }
        ScPan.setViewportView(lstLoadList);

    }

    private void setNextFile(int x) {
        try {
            lstLoadList.setSelectedIndex(x);
            lstLoadList.ensureIndexIsVisible(x);
        } catch (Exception e) {
        }
    }

    private void exitForm(int code) {
        if (code == 0) {
            dispose();
        } else {
            System.exit(1);
        }
    }

    private void DeleteFile(String FileName) {
        try {
            File delF = new File(FileName);
            if (delF.exists()) {
                delF.delete();
            }
            delF = null;
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "DeleteFile: " + e.getMessage());
            } else {
                log.append("DeleteFile: " + e.getMessage(), LogFilePath, "WCALoader");
            }
        }
    }

    private void GetTableList(String TabLst) {
        try {
            File sf = new File(TabLst);
            if (sf.exists()) {
                BufferedReader in = new BufferedReader(new FileReader(sf));
                String str = null;
                while ((str = in.readLine()) != null) {
                    TabList.add(str.trim());
                }
            }
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "GetTableList: " + e.getMessage());
            } else {
                log.append("GetTableList: " + e.getMessage(), LogFilePath, "WCALoader");
            }
        }
    }

    private boolean ChkTblLoad(String line) {
        try {
            String[] splt = line.split("\\|");
            if (splt[1] == "bochg" || splt[1] == "bschg" || splt[1] == "crchg" || splt[1] == "ctchg" || splt[1] == "ffc" || splt[1] == "frnfx" || splt[1] == "icc" || splt[1] == "ifchg" || splt[1] == "irchg" || splt[1] == "ischg" || splt[1] == "lcc" || splt[1] == "lstat" || splt[1] == "ltchg" || splt[1] == "mtchg" || splt[1] == "rconv" || splt[1] == "rdnom" || splt[1] == "scchg" || splt[1] == "shoch" || splt[1] == "trchg") {
                splt[1] = "Z" + splt[1];
            }
            if (TabList.size() > 0) {
                for (int x = 0; x < TabList.size(); x++) {
                    String tbl = "|" + TabList.get(x).toString().toUpperCase() + "|";
                    if (line.toUpperCase().startsWith(tbl)) {
                        Tblname = tbl;
                        LoadTbl = true;
                        return true;
                    }
                }
            }
            Tblname = splt[1];
            LoadTbl = false;
            return false;
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "ChkTblLoad: " + e.getMessage());
            } else {
                log.append("ChkTblLoad: " + e.getMessage(), LogFilePath, "WCALoader");
            }
            return false;
        }
    }

    //Get the record count and footer of the file
    private void GetRecordCount(String fname) {
        try {
            RecCnt = 0;
            File rfile = new File(fname);
            BufferedReader rin = new BufferedReader(new FileReader(rfile));
            String st = null;
            while ((st = rin.readLine()) != null) {
                RecCnt++;
            }
            rin.close();
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "GetRecordCount:" + e.getMessage());
            } else {
                log.append("GetRecordCount:" + e.getMessage(), LogFilePath, "WCALoader");
            }
        }
    }

    private boolean UnpackZipFile(String SourceFile, String unPackFolder) {
        FileInputStream fis = null;
        ZipInputStream sourceZipStream;
        FileOutputStream fos = null;
        ZipEntry theEntry = null;
        BufferedOutputStream targetStream = null;

        //String unPackFolder = Folder + "\\UNPACKED\\";
        // Loop though each Archive file		    
        try {
            // Assign the ZippedInputStream from validFiles.get(index)
            fis = new FileInputStream(new File(SourceFile));
            sourceZipStream = new ZipInputStream(fis);

            //  Loop though all Zip file Entries
            while ((theEntry = sourceZipStream.getNextEntry()) != null) {
                if (CheckDir(unPackFolder + theEntry.getName())) {
                    fos = new FileOutputStream(unPackFolder + theEntry.getName());
                    if (fos != null) {
                        targetStream = new BufferedOutputStream(fos, 2048);
                        int byteCount;
                        byte data[] = new byte[2048];

                        //  Read the compressed source stream and write to uncompressed stream
                        while ((byteCount = sourceZipStream.read(data, 0, 2048)) != -1) {
                            targetStream.write(data, 0, byteCount);
                        }
                    }
                }
                //  Close the target stream
                if (targetStream != null) {
                    targetStream.flush();
                    targetStream.close();
                }
                fos.close();
            }
            sourceZipStream.close();
            return true;
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "UnpackZipFile:" + e.getMessage());
            } else {
                log.append("UnpackZipFile:" + e.getMessage(), LogFilePath, "WCALoader");
            }
            return false;
        }
    }

    private boolean CheckDir(String fname) {
        try {
            String parent = (new File(fname).getParent());
            System.out.println(parent);
            File dir = new File(parent);
            if (dir.exists() == false) {
                return dir.mkdir();
            } else {
                return true;
            }
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "CheckDir:" + e.getMessage());
            } else {
                log.append("CheckDir:" + e.getMessage(), LogFilePath, "WCALoader");
            }
            return false;
        }
    }

    private void SendEmailAlert(String strTO, String sub, String strMessage) {
        try {
            /*EmailAlert alert = new EmailAlert();
            alert.SendAlert( strTO, strCC, sub, strMessage,false);*/
            gmailer alert = new gmailer();
            alert.Init("ops@exchange-data.com", "BLOCH1");
            alert.setRecipients(strTO);
            alert.setSubject(sub);
            alert.setMessageText(strMessage);
            alert.sendMessage();
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "SendEmailAlert:" + e.getMessage());
            } else {
                log.append("SendEmailAlert:" + e.getMessage(), LogFilePath, "WCALoader");
            }
        }
    }

    private void DeleteFiles(String folder) {
        try {
            File DirectoryListing = new File(folder);
            String[] DirContents = DirectoryListing.list();
            for (int x = 0; x < DirContents.length; x++) {
                File delF = new File(folder + DirContents[x]);
                if (delF.exists()) {
                    delF.delete();
                }
                delF = null;
            }
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "DeleteFiles:" + e.getMessage());
            } else {
                log.append("DeleteFiles:" + e.getMessage(), LogFilePath, "WCALoader");
            }
        }
    }

    private void tblExistsInDB(String line, MySql My, MsSql Ms) {
        Database db = new Database();
        String[] splt = line.split("\\|");

        String tblName = splt[1];

        db.setTblName(tblName);
        String dbName = "";
        int index = dbs.indexOf(db);
        if (index != -1) {
            dbName = dbs.get(index).getDbName();
            tblName = dbs.get(index).getTblName();

            if (My != null) {
                My.changeDB(dbName);
            } else {
                Ms.changeDB(dbName);
            }
        } else {
            if (My != null) {
                My.changeDB("wca");
            } else {
                Ms.changeDB("wca");
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new loader(args);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel;
    private javax.swing.JProgressBar ProgressBar;
    private javax.swing.JScrollPane ScPan;
    private javax.swing.JButton btLoad;
    private javax.swing.JLabel lblStatus;
    // End of variables declaration//GEN-END:variables

}
